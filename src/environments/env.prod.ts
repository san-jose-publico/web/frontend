export const environment = {
  production: true,
  apiURL: "http://csj-env.eba-fskcwcar.eu-west-3.elasticbeanstalk.com/api",
  recordsPerPage: 15,
};
