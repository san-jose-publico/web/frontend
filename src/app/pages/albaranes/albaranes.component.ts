import { Component, ElementRef, Renderer2, ViewChild } from '@angular/core';
import { Albaran } from 'src/app/interfaces/Albaran';
import { AlbaranService } from 'src/app/services/albaran.service';
import { MediaService } from 'src/app/services/media.service';
import { environment } from 'src/environments/env';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-albaranes',
  templateUrl: './albaranes.component.html',
  styleUrls: ['./albaranes.component.css']
})
export class AlbaranesComponent {
  albaranes: Albaran[] = [];
  orden: string = 'desc';
  tipo: string = 'fechaCreacion';
  loadCompleted: boolean = false;
  //pagination
  recordsTotal: number = 0;
  recordCurrent: number = 0;
  recordsPerPage: number = environment.recordsPerPage;

  //Busqueda
  searchResults: string[] = [];
  filterField: string = '';
  filterWord: string = '';
  startDateCalendar: string = '';
  endDateCalendar: string = '';
  filters: Map<string,string> = new Map([
    ['Número', 'numero'],
    ['Proveedor', 'proveedor'],
    ['M3', 'm3'],
    ['Tipo Hormigón', 'tipoHormigon'],
    ['Foto', 'foto'],
    ['Otro', 'otro'],
  ]);

  @ViewChild('searchInput') searchInput!: ElementRef;
  @ViewChild('calendar') calendar!: ElementRef;
  @ViewChild('startDate') startDate!: ElementRef;
  @ViewChild('endDate') endDate!: ElementRef;


  ngOnInit(): void {
    this.getAlbaranes();
  }


  getInitialInfo(){
    this.getAlbaranes();
  }

  //Paginacion
  changePage(page: number){
    page = (page < 0 ? 0 : page);
    this.recordCurrent = ((page - 1) * this.recordsPerPage >=0 ? (page - 1) * this.recordsPerPage : 0);
    this.getInitialInfo();
  }

  constructor(
    private mediaService: MediaService,
    private renderer2: Renderer2,
    private albaranService: AlbaranService,
  ) {
  }

  search(event: Event) {
    const query = (event.target as HTMLInputElement).value;
    const results = [];

    if (query.length > 0) {
        results.push(`<strong>Número:</strong> ${query}`);
        results.push(`<strong>Proveedor:</strong> ${query}`);
        results.push(`<strong>M3:</strong> ${query}`);
        results.push(`<strong>Tipo Hormigón:</strong> ${query}`);
        results.push(`<strong>Foto:</strong> ${query}`);
        results.push(`<strong>Otro:</strong> ${query}`);
    }
    else{
      this.filterField = '';
      this.filterWord = '';
      this.getInitialInfo();
    }
    this.searchResults = results;
  }

  async selectResult(result: string) {
    const regex = /<strong>(.*?)<\/strong>/;
    const matches = result.match(regex)!;
    let category = matches[1].replace(':', '');
    let query = this.searchInput.nativeElement.value;

    let filtro: string | undefined;
    filtro = this.filters.get(category);

    this.filterField =  filtro!;
    this.filterWord = query;

    this.searchResults = [];
    this.recordCurrent = 0;
    this.getInitialInfo();
  }


  async getAlbaranes(){
    return new Promise((resolve, reject) => {
      this.albaranService.getAlbaranes(this.recordCurrent, this.tipo, this.orden, this.startDateCalendar, this.endDateCalendar, this.filterField, this.filterWord)
      .subscribe({
        next: res => {
          this.albaranes = res.data;
          this.recordsTotal = res.recordsTotal;
          this.loadCompleted = true;
          resolve(res);
        },
        error: error => {
          reject(error);
          Swal.fire({icon: 'error', title: 'Oops...', text: 'No se pudo obtener los albaranes',});
        }
      });
    })
  }

  descargar(name: string){
    this.mediaService.getFile(name)
    .subscribe({
      next: res => {

        fetch(res).then(res => res.blob()).then(file => {
          let tempUrl = URL.createObjectURL(file);
          let aTag = document.createElement('a');
          aTag.href = tempUrl;
          aTag.download = name;
          document.body.appendChild(aTag);
          aTag.click();
          aTag.remove();
        })
      },
      error: error => {
        Swal.fire({icon: 'error', title: 'Oops...', text: 'No se pudo descargar el archivo',});
      }
    });
  }

  showElement(hide: boolean, elemento: HTMLDivElement, property?: string){
    let hidenClass = 'visibility-hidden';
    let shownClass = 'visibility-visible';

    if(property){
      hidenClass = 'display-none';
      shownClass = 'display-block';
    }

    if(hide){
      this.renderer2.removeClass(elemento, hidenClass);
      this.renderer2.addClass(elemento, shownClass);
    }
    else{
      this.renderer2.removeClass(elemento, shownClass);
      this.renderer2.addClass(elemento, hidenClass);
    }
  }

  ordenar(orden: string, tipo: string){
    this.orden = orden;
    this.tipo = tipo;
    this.getInitialInfo();
  }

  removeDate(){
    this.calendar.nativeElement.reset();
    this.showElement(false, this.calendar.nativeElement);

    this.startDate.nativeElement.value = '';
    this.endDate.nativeElement.value = '';
    this.startDateCalendar =  '';
    this.endDateCalendar = '';

    this.getInitialInfo();
  }

  searchDate(){
    this.startDateCalendar =  this.startDate.nativeElement.value;
    this.endDateCalendar = this.endDate.nativeElement.value;
    this.showElement(false, this.calendar.nativeElement);

    this.getInitialInfo();
  }

  openCalendar(){
    this.showElement(true, this.calendar.nativeElement);
  }

  closeCalendar(){
    this.showElement(false, this.calendar.nativeElement);
  }
}
