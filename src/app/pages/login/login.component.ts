import { Component, ElementRef, OnInit, Renderer2, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { of, switchMap, tap } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';
import { UsuarioService } from 'src/app/services/user.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit{
  loginForm: FormGroup;

  @ViewChild('aviso') aviso!: ElementRef;

  ngOnInit(): void {
  }

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private authService: AuthService,
    private usuarioService: UsuarioService,
    private renderer2: Renderer2

  ){
    this.loginForm = this.fb.group({
      username: [localStorage.getItem('remember') === 'true' ? localStorage.getItem('username') : '', [ Validators.required, Validators.email]],
      password: ['', Validators.required ],
      // remember: [localStorage.getItem('remember') === 'true' ? true : false, Validators.required ]
    });
  }


  login() {
    this.authService.login(this.loginForm.value)
      .pipe(
        tap(res => {
          this.guardarLocal(res);
        }),
        switchMap(res => {
          this.actualizarUltimoAcceso(res.uid);
          return of(res); // Devuelve un observable con el resultado original para mantener la cadena
        })
      )
      .subscribe({
        next: res => {
          sessionStorage.setItem('sesion', 'true');
        },
        error: error => {
          this.renderer2.removeClass(this.aviso.nativeElement, 'visibility-hidden');
          this.renderer2.addClass(this.aviso.nativeElement, 'visibility-visible');
        }
      });
  }

  guardarLocal(res: any){
    localStorage.setItem('token', res.token);
    localStorage.setItem('uid', res.uid);
    localStorage.setItem('rol', res.rol);
    // localStorage.setItem('remember',  this.loginForm.get('remember')!.value.toString());
    localStorage.setItem('username', this.loginForm.get('username')!.value);
  }

  actualizarUltimoAcceso(uid: string){
    const data = {
      ultimoAcceso: new Date()
    }
    this.usuarioService.actualizarUltimoAcceso(uid, data)
    .subscribe({
      next: () => {
        this.router.navigate(['home']);
      },
      error: error => {
        Swal.fire({icon: 'error', title: 'Oops...', text: 'No se pudo actualizar el último acceso',});
      }
    });
  }

}
