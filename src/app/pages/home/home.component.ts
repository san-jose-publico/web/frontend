import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit{
  admin: boolean = false;

  ngOnInit(): void {
    this.checkRol();
  }

  constructor(
    private router: Router,
    private authService: AuthService
  ){}

  ver(ruta: string){
    this.router.navigate([ruta]);
  }

  checkRol(){
    if(this.authService.getRol() === 'admin'){
      this.admin = true;
    }
  }

}
