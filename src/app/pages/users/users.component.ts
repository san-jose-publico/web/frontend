import { DOCUMENT } from '@angular/common';
import { Component, ElementRef, Inject, OnInit, Renderer2, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Especialidad } from 'src/app/interfaces/Especialidad';
import { Usuario } from 'src/app/interfaces/Usuario';
import { EspecialidadService } from 'src/app/services/especialidad.service';
import { UsuarioService } from 'src/app/services/user.service';
import { environment } from 'src/environments/env';
import Swal from 'sweetalert2';

type MensajeAccion = 'crear' | 'eliminar' | 'editar' | 'activar' | 'desactivar';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit{
  usuarios: Usuario[] = [];
  orden: string = 'desc';
  tipo: string = 'ultimoAcceso';
  userForm: FormGroup;
  userCopy!: Usuario;
  create: boolean = false;
  formInvalid: boolean = false;
  especialidadValue: string = '';
  especialidades: Especialidad[] = [];
  loadCompleted: boolean = false;
  //pagination
  recordsTotal: number = 0;
  recordCurrent: number = 0;
  recordsPerPage: number = environment.recordsPerPage;

  accionesMensajes: Record<MensajeAccion, string> = {
    crear: 'El usuario se ha creado con éxito',
    eliminar: 'El usuario se ha eliminado con éxito',
    editar: 'El usuario se ha actualizado con éxito',
    activar: 'El usuario se ha activado con éxito',
    desactivar: 'El usuario se ha desactivado con éxito',
  };

  //Busqueda
  searchResults: string[] = [];
  filterField: string = '';
  filterWord: string = '';
  startDateCalendar: string = '';
  endDateCalendar: string = '';
  filters: Map<string,string> = new Map([
    ['Usuario', 'username'],
    ['Email', 'email'],
    ['Nombre', 'nombre'],
    ['Apellidos', 'apellidos'],
  ]);

  @ViewChild('modal') modal!: ElementRef;
  @ViewChild('newPassword') newPassword!: ElementRef;
  @ViewChild('especialidad') especialidad!: ElementRef;

  @ViewChild('searchInput') searchInput!: ElementRef;
  @ViewChild('calendar') calendar!: ElementRef;
  @ViewChild('startDate') startDate!: ElementRef;
  @ViewChild('endDate') endDate!: ElementRef;

  constructor(
    private usuarioService: UsuarioService,
    private renderer2: Renderer2,
    private fb: FormBuilder,
    private especialidadService: EspecialidadService,
    @Inject(DOCUMENT) private document: Document
  ) {
    this.userForm = this.fb.group({
      username: ['', Validators.required],
      nombre: ['', Validators.required],
      apellidos: ['', Validators.required],
      rol: ['', Validators.required],
      especialidad: ['', Validators.required],
      email: ['', [ Validators.required, Validators.email]],
      password: ['']
    });
  }

  ngOnInit(): void {
    this.getInitialInfo();
  }

  //Paginacion
  changePage(page: number){
    page = (page < 0 ? 0 : page);
    this.recordCurrent = ((page - 1) * this.recordsPerPage >=0 ? (page - 1) * this.recordsPerPage : 0);
    this.getInitialInfo();
  }

  getInitialInfo(){
    this.getUsuarios();
    this.getEspecialidades();
  }

  search(event: Event) {
    const query = (event.target as HTMLInputElement).value;
    const results = [];

    if (query.length > 0) {
        results.push(`<strong>Usuario:</strong> ${query}`);
        results.push(`<strong>Email:</strong> ${query}`);
        results.push(`<strong>Nombre:</strong> ${query}`);
        results.push(`<strong>Apellidos:</strong> ${query}`);
    }
    else{
      this.filterField = '';
      this.filterWord = '';
      this.getInitialInfo();
    }
    this.searchResults = results;
  }

  async selectResult(result: string) {
    const regex = /<strong>(.*?)<\/strong>/;
    const matches = result.match(regex)!;
    let category = matches[1].replace(':', '');
    let query = this.searchInput.nativeElement.value;

    let filtro: string | undefined;
    filtro = this.filters.get(category);

    this.filterField =  filtro!;
    this.filterWord = query;

    this.searchResults = [];
    this.recordCurrent = 0;
    this.getInitialInfo();
  }

  getUsuarios(){
    this.usuarioService.getUsuarios(this.recordCurrent, this.tipo, this.orden, this.startDateCalendar, this.endDateCalendar, this.filterField, this.filterWord)
    .subscribe({
      next: res => {
       this.usuarios = res.data;
       this.recordsTotal = res.recordsTotal;
       this.loadCompleted = true;
      },
      error: error => {
        Swal.fire({icon: 'error', title: 'Oops...', text: 'No se pudo obtener los usuarios',});
      }
    });
  }

  async getEspecialidades(){
    return new Promise((resolve, reject) => {
      this.especialidadService.getEspecialidades()
      .subscribe({
        next: res => {
          this.especialidades = res.data;
          resolve(res);
        },
        error: error => {
          reject(error);
          Swal.fire({icon: 'error', title: 'Oops...', text: 'No se pudo obtener las especialidades',});
        }
      });
    })
  }

  ordenar(orden: string, tipo: string){
    this.orden = orden;
    this.tipo = tipo;
    this.getInitialInfo();
  }

  openEditForm(usuario: Usuario){
    this.newPassword.nativeElement.value = '';
    this.userCopy = usuario;
    this.userForm.patchValue({
      username: usuario.username,
      nombre: usuario.nombre,
      apellidos: usuario.apellidos,
      rol: usuario.rol,
      especialidad: usuario.especialidad,
      email: usuario.email
    });

    this.showElement(true, this.modal.nativeElement);
  }

  hideForm(){
    this.formInvalid = false;

    if(this.create){
      this.create = false;
    }

    this.showElement(false, this.modal.nativeElement)
    this.document.body.style.overflow = 'auto';
  }

  editUser(){
    this.userForm.patchValue({
      password: this.newPassword.nativeElement.value ? this.newPassword.nativeElement.value : this.userCopy.password,
    });

    this.usuarioService.actualizarUsuario(this.userCopy.uid, this.userForm.value)
    .subscribe({
      next: () => {
        this.hideForm();
        this.successMessage('editar');
        this.userForm.markAsPristine();
        this.getInitialInfo();
      },
      error: error => {
        Swal.fire({icon: 'error', title: 'Oops...', text: 'No se pudo completar la acción, vuelva a intentarlo',});
      }
    });
  }

  openEmptyForm(){
    this.userForm.reset();
    this.create = true;
    this.showElement(true, this.modal.nativeElement);
  }

  createUser(){
    if(this.userForm.valid){
      this.usuarioService.createUser(this.userForm.value)
      .subscribe({
        next: () => {
          this.hideForm();
          this.successMessage('crear');
          this.userForm.markAsPristine();
          this.getInitialInfo();
          this.create = false;
        },
        error: error => {
          Swal.fire({icon: 'error', title: 'Oops...', text: 'No se pudo completar la acción, vuelva a intentarlo',});
        }
      });
    }
    else{
      this.formInvalid = true;
    }
  }

  enableUser(uid: string, nombre: string, apellidos: string, activity: boolean){
    let status: boolean;
    let action, title;
    if(activity){
      status = false;
      action = 'desactivar';
      title = 'Desactivar';
    }
    else{
      status = true;
      action = 'activar';
      title = 'Activar';
    }

    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-secondary me-4'
      },
      buttonsStyling: false
    })
    swalWithBootstrapButtons.fire({
      title: `${title} usuario`,
      text: `Se va a ${action} el usuario ${nombre} ${apellidos}. ¿Desea continuar?`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Aceptar',
      cancelButtonText: 'Cancelar',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {

        const body = {
          activo: status
        }
        this.usuarioService.statusUser(uid, body)
        .subscribe({
          next: () => {
            this.hideForm();
            if(activity){
              this.successMessage('desactivar');
            }
            else{
              this.successMessage('activar');
            }
            this.userForm.markAsPristine();
            this.getUsuarios();
          },
          error: error => {
            Swal.fire({icon: 'error', title: 'Oops...', text: 'No se pudo dar de baja al usuario, vuelva a intentarlo',});
          }
        });
      }
    });
  }

  successMessage(accion: MensajeAccion) {
    Swal.fire({
      position: 'center',
      icon: 'success',
      title: this.accionesMensajes[accion],
      showConfirmButton: false,
      timer: 1500
    });
  }

  showElement(hide: boolean, elemento: HTMLDivElement, property?: string){
    let hidenClass = 'visibility-hidden';
    let shownClass = 'visibility-visible';

    if(property){
      hidenClass = 'display-none';
      shownClass = 'display-block';
    }

    if(hide){
      this.renderer2.removeClass(elemento, hidenClass);
      this.renderer2.addClass(elemento, shownClass);
    }
    else{
      this.renderer2.removeClass(elemento, shownClass);
      this.renderer2.addClass(elemento, hidenClass);
    }
  }

  async addSpeciality(){
    let especialidad: Especialidad = { nombre: this.especialidadValue }
    await this.createEspecialidad(especialidad);
    this.getEspecialidades();
    this.clearInput();
  }

  async deleteSpeciality(){
    await this.getEspecialidades();

    const especialidad = this.especialidades.find((especialidad) => especialidad.nombre ===  this.especialidadValue);
    if(especialidad){
      await this.deleteEspecialidad(especialidad.uid!);
      this.getEspecialidades();
    }

    this.clearInput();
  }

  async createEspecialidad(especialidad: Especialidad){
    return new Promise((resolve, reject) => {
      this.especialidadService.createEspecialidad(especialidad)
      .subscribe({
        next: res => {
          resolve(res);
        },
        error: error => {
          reject(error);
          if(error.status === 500){
            Swal.fire({icon: 'error', title: 'Oops...', text: 'No se pudo crear la especialidad',});
          }
          else if(error.status === 400){
            Swal.fire({icon: 'error', title: 'Oops...', text: 'El nombre ya existe',});
          }
        }
      });
    })
  }

  async deleteEspecialidad(uid: string){
    return new Promise((resolve, reject) => {
      this.especialidadService.deleteEspecialidad(uid)
      .subscribe({
        next: res => {
          resolve(res);
        },
        error: error => {
          reject(error);
          Swal.fire({icon: 'error', title: 'Oops...', text: 'No se pudo eliminar la especialidad',});
        }
      });
    })
  }

  lowerCase(event: any): void {
    const inputValueLowerCase = event.target.value.toLowerCase();
    this.renderer2.setProperty(event.target, 'value', inputValueLowerCase);
    this.especialidadValue = inputValueLowerCase;
  }

  clearInput() {
    this.especialidad.nativeElement.value = '';
  }

  removeDate(){
    this.calendar.nativeElement.reset();
    this.showElement(false, this.calendar.nativeElement);

    this.startDate.nativeElement.value = '';
    this.endDate.nativeElement.value = '';
    this.startDateCalendar =  '';
    this.endDateCalendar = '';

    this.getInitialInfo();
  }

  searchDate(){
    this.startDateCalendar =  this.startDate.nativeElement.value;
    this.endDateCalendar = this.endDate.nativeElement.value;
    this.showElement(false, this.calendar.nativeElement);

    this.getInitialInfo();
  }

  openCalendar(){
    this.showElement(true, this.calendar.nativeElement);
  }

  closeCalendar(){
    this.showElement(false, this.calendar.nativeElement);
  }
}
