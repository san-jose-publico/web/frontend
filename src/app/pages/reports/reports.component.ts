import { Component, ElementRef, OnInit, QueryList, Renderer2, ViewChild, ViewChildren } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Incidencia } from 'src/app/interfaces/Incidencia';
import { Inspeccion } from 'src/app/interfaces/Inspeccion';
import { Proyecto } from 'src/app/interfaces/Proyecto';
import { Usuario } from 'src/app/interfaces/Usuario';
import { IncidenciaService } from 'src/app/services/incidencia.service';
import { InspeccionService } from 'src/app/services/inspeccion.service';
import { ProyectoService } from 'src/app/services/proyecto.service';
import { UsuarioService } from 'src/app/services/user.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.css']
})
export class ReportsComponent implements OnInit{
[x: string]: any;
  proyecto!: Proyecto;
  cargado: boolean = false;
  incidencias: Incidencia[] = [];
  inspecciones: Inspeccion[] = [];
  oldPosition: number = -1;
  usuarios: Usuario[] = [];
  ordenIncidencia: string = 'desc';
  tipoIncidencia: string = 'fechaCreacion';
  ordenInspeccion: string = 'desc';
  tipoInspeccion: string = 'fechaCreacion';
  rowSelected: boolean = false;

  //Busqueda
  searchResults: string[] = [];
  filterFieldIncidencia: string = '';
  filterWordIncidencia: string = '';
  filterFieldInspeccion: string = '';
  filterWordInspeccion: string = '';
  startDateCalendar: string = '';
  endDateCalendar: string = '';
  filters: Map<string,string> = new Map([
    ['Código', 'codigo'],
    ['Usuario', 'autor'],
    ['Descripción', 'descripcion'],
  ]);

  @ViewChildren('row') row!: QueryList<any>;

  @ViewChild('searchInput') searchInput!: ElementRef;
  @ViewChild('calendar') calendar!: ElementRef;
  @ViewChild('startDate') startDate!: ElementRef;
  @ViewChild('endDate') endDate!: ElementRef;

  ngOnInit(): void {
    this.getInitialInfo();
  }

  constructor(
    private proyectoService: ProyectoService,
    private incidenciaService: IncidenciaService,
    private inspeccionService: InspeccionService,
    private usuarioService: UsuarioService,
    private renderer2: Renderer2,
    private route: ActivatedRoute,
  ){}


  async getInitialInfo(){
    await this.getProyecto();
    this.getIncidencias();
    this.getInspecciones();
  }

  search(event: Event) {
    const query = (event.target as HTMLInputElement).value;
    const results = [];

    if (query.length > 0) {
        results.push(`<strong>Código:</strong> ${query}`);
        results.push(`<strong>Usuario:</strong> ${query}`);
        results.push(`<strong>Descripción:</strong> ${query}`);
        results.push(`<strong>Código:</strong> ${query}`);
        results.push(`<strong>Usuario:</strong> ${query}`);
        results.push(`<strong>Descripción:</strong> ${query}`);
    }
    else{
      this.filterFieldIncidencia = '';
      this.filterWordIncidencia = '';
      this.filterFieldInspeccion = '';
      this.filterWordInspeccion = '';
      this.getInitialInfo();
    }
    this.searchResults = results;
  }

  async selectResult(result: string, iteration: number) {
    const regex = /<strong>(.*?)<\/strong>/;
    const matches = result.match(regex)!;
    let category = matches[1].replace(':', '');
    let query = this.searchInput.nativeElement.value;

    if(category === 'Usuario'){
      await this.getUsuarios();
      query = this.getIdElement(query, this.usuarios);
    }

    let filtro: string | undefined;
    filtro = this.filters.get(category);

    if(iteration === 0 || iteration === 1 || iteration === 2){
      this.filterFieldIncidencia =  filtro!;
      this.filterWordIncidencia = query;
    }
    else{
      this.filterFieldInspeccion =  filtro!;
      this.filterWordInspeccion = query;
    }

    this.searchResults = [];
    this.getInitialInfo();
  }

  getIdElement(nombre: string, arrayList: Array<Usuario>): string[] | string {
    const normalizedName = nombre.normalize("NFD").replace(/[\u0300-\u036f]/g, "").toLowerCase();
    const words = normalizedName.split(' ');
    const regex = new RegExp(`(${this.escapeRegExp(words.join('|'))})`, "i");
    let idArray;

    idArray = arrayList
    .filter((usuario) => {
      const normalizedName = this.normalizeWord(usuario.nombre);
      const normalizedUsername = this.normalizeWord(usuario.apellidos);
      return regex.test(normalizedName) || regex.test(normalizedUsername);
    })
    .map((element) => element.uid);

    return idArray.length > 0 ? idArray : '';
  }

  normalizeWord(word: string): string{
    return word.normalize("NFD").replace(/[\u0300-\u036f]/g, "").toLowerCase();
  }

  escapeRegExp(string: string): string{
    return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
  }

  async getUsuarios() {
    return new Promise((resolve, reject) => {
      this.usuarioService.getUsuarios()
        .subscribe({
          next: res => {
            this.usuarios = res.data;
            resolve(res);
          },
          error: error => {
            reject(error);
            Swal.fire({ icon: 'error', title: 'Oops...', text: 'No se pudo obtener los usuarios', });
          }
        });
    })
  }

  async getProyecto(){
    let proyectoId = '';

    if(this.route.snapshot.queryParamMap.get('proyecto')){
      proyectoId = this.route.snapshot.queryParamMap.get('proyecto')!;
    }

    return new Promise((resolve, reject) => {
      this.proyectoService.getProyecto(proyectoId)
      .subscribe({
        next: res => {
          this.proyecto = res.data;
          resolve(res);
        },
        error: error => {
          reject(error);
          Swal.fire({icon: 'error', title: 'Oops...', text: 'No se pudo obtener los proyectos',});
        }
      });
    })
  }

  getIncidencias(){
    this.incidenciaService.getIncidenciasProyecto(this.proyecto.uid, this.tipoIncidencia, this.ordenIncidencia, this.startDateCalendar, this.endDateCalendar, this.filterFieldIncidencia, this.filterWordIncidencia)
    .subscribe({
      next: async res => {
        this.incidencias = res.data;

        const promises = this.incidencias.map(async (incidencia) => {
          incidencia.autorNombre = await this.getNombreUsuario(incidencia.autor);
        });

        await Promise.all(promises);
      // this.loadCompleted = true;
      },
      error: error => {
        Swal.fire({icon: 'error', title: 'Oops...', text: 'No se pudo obtener las incidencias del proyecto',});
      }
    });
  }

  async getNombreUsuario(uid: string): Promise<string> {
    return new Promise((resolve, reject) => {
      this.usuarioService.getUsuario(uid)
        .subscribe({
          next: res => {
            resolve(res.data.nombre + ' ' + res.data.apellidos);
          },
          error: error => {
            reject(error);
            Swal.fire({ icon: 'error', title: 'Oops...', text: 'No se pudo obtener los nombres de usuarios', });
          }
        });
    })
  }

  getInspecciones(){
    this.inspeccionService.getInspeccionesProyecto(this.proyecto.uid, this.tipoInspeccion, this.ordenInspeccion, this.startDateCalendar, this.endDateCalendar, this.filterFieldInspeccion, this.filterWordInspeccion)
    .subscribe({
      next: async res => {
        this.inspecciones = res.data;
        const promises = this.inspecciones.map(async (inspeccion) => {
          inspeccion.autorNombre = await this.getNombreUsuario(inspeccion.autor);
        });

        await Promise.all(promises);
      },
      error: error => {
        Swal.fire({icon: 'error', title: 'Oops...', text: 'No se pudo obtener las inspecciones del proyecto',});
      }
    });
  }

  selectInspecciones(uid: string, position: number){
    const classToAdd = 'table-active';
    const element = this.row.get(position).nativeElement;

    if(this.rowSelected && this.oldPosition === position){
      this.getInspecciones();
      this.renderer2.removeClass(element, classToAdd);
      const oldElement = this.row.get(this.oldPosition).nativeElement;
      this.renderer2.removeClass(oldElement, classToAdd);
      this.rowSelected = false;
    }
    else{
      this.rowSelected = true;
      this.selectRow(position);
      this.getInspeccionesFromIncidencia(uid);
    }
    this.oldPosition = position;

  }

  selectRow(position: number){
    const classToAdd = 'table-active';

    if(this.oldPosition !== -1){
      const oldElement = this.row.get(this.oldPosition).nativeElement;
      this.renderer2.removeClass(oldElement, classToAdd);
    }

    const element = this.row.get(position).nativeElement;
    this.renderer2.addClass(element, classToAdd);
  }


  getInspeccionesFromIncidencia(uid: string){
    this.incidenciaService.getInspeccionesFromIncidencia(uid, this.tipoInspeccion, this.ordenInspeccion, this.startDateCalendar, this.endDateCalendar, this.filterFieldInspeccion, this.filterWordInspeccion)
    .subscribe({
      next: async res => {
        this.inspecciones = res.data;
        const promises = this.inspecciones.map(async (inspeccion) => {
          inspeccion.autorNombre = await this.getNombreUsuario(inspeccion.autor);
        });
        await Promise.all(promises);
      },
      error: error => {
        Swal.fire({icon: 'error', title: 'Oops...', text: 'No se pudo obtener las inspecciones de la incidencia',});
      }
    });
  }

  showElement(hide: boolean, elemento: HTMLDivElement, property?: string){
    let hidenClass = 'visibility-hidden';
    let shownClass = 'visibility-visible';

    if(property){
      hidenClass = 'display-none';
      shownClass = 'display-block';
    }

    if(hide){
      this.renderer2.removeClass(elemento, hidenClass);
      this.renderer2.addClass(elemento, shownClass);
    }
    else{
      this.renderer2.removeClass(elemento, shownClass);
      this.renderer2.addClass(elemento, hidenClass);
    }
  }

  ordenar(orden: string, tipo: string, dato: string){
    if(dato === 'incidencia'){
      this.ordenIncidencia = orden;
      this.tipoIncidencia = tipo;

      this.getIncidencias();
    }
    else{
      this.ordenInspeccion = orden;
      this.tipoInspeccion = tipo;

      this.getInspecciones();
    }
  }

  removeDate(){
    this.calendar.nativeElement.reset();
    this.showElement(false, this.calendar.nativeElement);

    this.startDate.nativeElement.value = '';
    this.endDate.nativeElement.value = '';
    this.startDateCalendar =  '';
    this.endDateCalendar = '';

    this.getInitialInfo();
  }

  searchDate(){
    this.startDateCalendar =  this.startDate.nativeElement.value;
    this.endDateCalendar = this.endDate.nativeElement.value;
    this.showElement(false, this.calendar.nativeElement);

    this.getInitialInfo();
  }

  openCalendar(){
    this.showElement(true, this.calendar.nativeElement);
  }

  closeCalendar(){
    this.showElement(false, this.calendar.nativeElement);
  }
}
