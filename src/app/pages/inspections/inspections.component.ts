import { Component, ElementRef, OnInit, QueryList, Renderer2, ViewChild, ViewChildren } from '@angular/core';
import { firstValueFrom } from 'rxjs';
import { Inspeccion } from 'src/app/interfaces/Inspeccion';
import { IncidenciaService } from 'src/app/services/incidencia.service';
import { InspeccionService } from 'src/app/services/inspeccion.service';
import { MediaService } from 'src/app/services/media.service';
import Swal from 'sweetalert2';
import * as pdfMake from 'pdfmake/build/pdfmake';
import * as pdfFonts from "pdfmake/build/vfs_fonts";
import { EspecialidadService } from 'src/app/services/especialidad.service';
import { Especialidad } from 'src/app/interfaces/Especialidad';
import { UsuarioService } from 'src/app/services/user.service';
import { HttpClient } from '@angular/common/http';
import { Usuario } from 'src/app/interfaces/Usuario';
import { ProyectoService } from 'src/app/services/proyecto.service';
import { Incidencia } from 'src/app/interfaces/Incidencia';
import { environment } from 'src/environments/env';

(<any>pdfMake).vfs = pdfFonts.pdfMake.vfs;

@Component({
  selector: 'app-inspections',
  templateUrl: './inspections.component.html',
  styleUrls: ['./inspections.component.css']
})
export class InspectionsComponent implements OnInit {
  inspecciones: Inspeccion[] = [];
  orden: string = 'desc';
  tipo: string = 'fechaCreacion';
  loadCompleted: boolean = false;
  logoText!: string;
  inspeccionAviso!: Inspeccion;
  especialidades: Especialidad[] = [];
  inspeccionUrlPics: string[] = [];
  incidencias: Incidencia[] = [];
  usuarios: Usuario[] = [];
  notificationData!: Inspeccion;

  //pagination
  recordsTotal: number = 0;
  recordCurrent: number = 0;
  recordsPerPage: number = environment.recordsPerPage;

  //Busqueda
  searchResults: string[] = [];
  filterField: string = '';
  filterWord: string = '';
  startDateCalendar: string = '';
  endDateCalendar: string = '';
  filters: Map<string,string> = new Map([
    ['Código', 'codigo'],
    ['Incidencia', 'incidencia'],
    ['Usuario', 'autor'],
    ['Descripción', 'descripcion'],
    ['C1', 'complemento1'],
    ['C2', 'complemento2'],
  ]);

  @ViewChildren('op') op!: QueryList<any>;
  @ViewChild('avisoInspeccion') avisoInspeccion!: ElementRef;

  @ViewChild('searchInput') searchInput!: ElementRef;
  @ViewChild('calendar') calendar!: ElementRef;
  @ViewChild('startDate') startDate!: ElementRef;
  @ViewChild('endDate') endDate!: ElementRef;

  ngOnInit(): void {
    this.getInitialInfo();
    this.loadImageFromBase64File();
  }

  constructor(
    private inspeccionService: InspeccionService,
    private mediaService: MediaService,
    private incidenciaService: IncidenciaService,
    private renderer2: Renderer2,
    private especialidadService: EspecialidadService,
    private usuarioService: UsuarioService,
    private http: HttpClient,
    private proyectoService: ProyectoService
  ) {
  }

  getInitialInfo(){
    this.getInspecciones();
  }

  //Paginacion
  changePage(page: number){
    page = (page < 0 ? 0 : page);
    this.recordCurrent = ((page - 1) * this.recordsPerPage >=0 ? (page - 1) * this.recordsPerPage : 0);
    this.getInitialInfo();
  }

  search(event: Event) {
    const query = (event.target as HTMLInputElement).value;
    const results = [];

    if (query.length > 0) {
        results.push(`<strong>Código:</strong> ${query}`);
        results.push(`<strong>Incidencia:</strong> ${query}`);
        results.push(`<strong>Usuario:</strong> ${query}`);
        results.push(`<strong>Descripción:</strong> ${query}`);
        results.push(`<strong>C1:</strong> ${query}`);
        results.push(`<strong>C2:</strong> ${query}`);
    }
    else{
      this.filterField = '';
      this.filterWord = '';
      this.getInitialInfo();
    }
    this.searchResults = results;
  }

  async selectResult(result: string) {
    const regex = /<strong>(.*?)<\/strong>/;
    const matches = result.match(regex)!;
    let category = matches[1].replace(':', '');
    let query = this.searchInput.nativeElement.value;

    if(category === 'Incidencia'){
      await this.getIncidencias();
      query = this.getIdElement(query, this.incidencias);
    }
    else if(category === 'Usuario'){
      await this.getUsuarios();
      query = this.getIdElement(query, this.usuarios);
    }

    let filtro: string | undefined;
    filtro = this.filters.get(category);

    this.filterField =  filtro!;
    this.filterWord = query;

    this.searchResults = [];
    this.recordCurrent = 0;
    this.getInitialInfo();
  }

  getIdElement(nombre: string, arrayList: Array<Incidencia | Usuario>): string[] | string {
    const normalizedName = nombre.normalize("NFD").replace(/[\u0300-\u036f]/g, "").toLowerCase();
    const words = normalizedName.split(' ');
    const regex = new RegExp(`(${this.escapeRegExp(words.join('|'))})`, "i");
    let idArray;

    if('email' in arrayList[0]){
      idArray = arrayList
      .filter((element): element is Usuario => {
        return 'nombre' in element && 'apellidos' in element;
      })
      .filter((usuario) => {
        const normalizedName = this.normalizeWord(usuario.nombre);
        const normalizedUsername = this.normalizeWord(usuario.apellidos);
        return regex.test(normalizedName) || regex.test(normalizedUsername);
      })
      .map((usuario) => usuario.uid);
    }
    else{
      idArray = arrayList
      .filter((element): element is Incidencia => {
        return 'codigo' in element;
      })
      .filter((element) => {
        const normalizedName = this.normalizeWord(element.codigo);
        return regex.test(normalizedName);
      })
      .map((element) => element.uid);
    }

    return idArray.length > 0 ? idArray : '';
  }

  normalizeWord(word: string): string{
    return word.normalize("NFD").replace(/[\u0300-\u036f]/g, "").toLowerCase();
  }

  escapeRegExp(string: string): string{
    return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
  }

  async getIncidencias(): Promise<Incidencia[]> {
    return new Promise((resolve, reject) => {
      this.incidenciaService.getIncidenciasAll()
        .subscribe({
          next: res => {
            this.incidencias = res.data;
            resolve(res);
          },
          error: error => {
            reject(error);
            Swal.fire({ icon: 'error', title: 'Oops...', text: 'No se pudo obtener las incidencias', });
          }
        });
    })
  }

  async getUsuarios() {
    return new Promise((resolve, reject) => {
      this.usuarioService.getUsuarios()
        .subscribe({
          next: res => {
            this.usuarios = res.data;
            resolve(res);
          },
          error: error => {
            reject(error);
            Swal.fire({ icon: 'error', title: 'Oops...', text: 'No se pudo obtener los usuarios', });
          }
        });
    })
  }

  loadImageFromBase64File() {
    this.http.get('assets/logo.txt', {responseType: 'text'})
    .subscribe(data => {
      this.logoText = data;
    });
  }

  descargar(name: string){
    this.mediaService.getFile(name)
    .subscribe({
      next: res => {

        fetch(res).then(res => res.blob()).then(file => {
          let tempUrl = URL.createObjectURL(file);
          let aTag = document.createElement('a');
          aTag.href = tempUrl;
          aTag.download = name;
          document.body.appendChild(aTag);
          aTag.click();
          aTag.remove();
        })
      },
      error: error => {
        Swal.fire({icon: 'error', title: 'Oops...', text: 'No se pudo descargar el archivo',});
      }
    });
  }

  async getEspecialidades(){
    return new Promise((resolve, reject) => {
      this.especialidadService.getEspecialidades()
      .subscribe({
        next: res => {
          this.especialidades = res.data;
          resolve(res);
        },
        error: error => {
          reject(error);
          Swal.fire({icon: 'error', title: 'Oops...', text: 'No se pudo obtener las especialidades',});
        }
      });
    })
  }

  async getInspecciones() {
    try{
      const res = await firstValueFrom(this.inspeccionService.getInspecciones(this.recordCurrent, this.tipo, this.orden, this.startDateCalendar, this.endDateCalendar, this.filterField, this.filterWord));
      this.inspecciones = res.data;
      this.recordsTotal = res.recordsTotal;

      if(this.inspecciones && this.inspecciones.length > 0){
        const promises = this.inspecciones.map(async (inspeccion) => {
          inspeccion.incidenciaCod = await this.getCodIncidencia(inspeccion.incidencia);
          inspeccion.proyectoCod = await this.getNombreProyecto(inspeccion.proyecto);
          inspeccion.autorNombre = await this.getNombreUsuario(inspeccion.autor);
        });

        await Promise.all(promises);
      }
      this.loadCompleted = true;
    }
    catch (error) {
      Swal.fire({ icon: 'error', title: 'Oops...', text: 'No se pudo obtener las inspecciones', });
    }
  }

  async getCodIncidencia(uid: string): Promise<string> {
    try {
      const res = await firstValueFrom(this.incidenciaService.getIncidencia(uid));
      return res.data.codigo;
    } catch (error) {
      Swal.fire({ icon: 'error', title: 'Oops...', text: 'No se pudo obtener el código de la incidencia', });
      throw error;
    }
  }

  async getNombreProyecto(uid: string): Promise<string> {
    try {
      const res = await firstValueFrom(this.proyectoService.getProyecto(uid));
      return res.data.codigo;
    } catch (error) {
      Swal.fire({ icon: 'error', title: 'Oops...', text: 'No se pudo obtener el código del proyecto', });
      throw error;
    }
  }

  async getNombreUsuario(uid: string): Promise<string> {
    return new Promise((resolve, reject) => {
      this.usuarioService.getUsuario(uid)
        .subscribe({
          next: res => {
            resolve(res.data.nombre + ' ' + res.data.apellidos);
          },
          error: error => {
            reject(error);
            Swal.fire({ icon: 'error', title: 'Oops...', text: 'No se pudo obtener los nombres de usuarios', });
          }
        });
    })
  }

  async getLink(archivo: string): Promise<string> {
    return new Promise<string>((resolve, reject) => {
      this.mediaService.getFile(archivo)
      .subscribe({
        next: res => {
          resolve(res);
        },
        error: error => {
          reject(error);
        }
      });
    });
  }

  async downloadPdf(inspeccion: Inspeccion) {
    let pages = [];
    let bodies = [];

    this.inspeccionUrlPics = await this.convertBase64(inspeccion);
    const objectData = await this.setPdfSettings(inspeccion);
    const body = await this.setBody(inspeccion, objectData);
    bodies.push(body);
    const page =  await this.setPageContent(body);
    pages.push(page);
    this.printPdf(pages);
  }

  //Devuelve las imagenes en base64
  async convertBase64(inspeccion: Inspeccion): Promise<string[]>{
    const fotosBase64: string[] = [];

    if (inspeccion.documentos.fotos) {
      const fotos = await Promise.all(
        inspeccion.documentos.fotos.map(async (foto) => await this.getLink(foto))
      );

      fotosBase64.push(
        ...(await Promise.all(
          fotos.map(
            async (foto) =>
              await new Promise<string>((resolve) => {
                const img = new Image();
                img.src = foto;
                img.crossOrigin = 'Anonymous';

                img.onload = () => {
                  const canvas = document.createElement('canvas');
                  canvas.width = img.width;
                  canvas.height = img.height;

                  const ctx = canvas.getContext('2d')!;
                  ctx.drawImage(img, 0, 0);

                  const dataURL = canvas.toDataURL('image/PNG');
                  resolve(dataURL);
                };
              })
          )
        ))
      );
    }
    return fotosBase64;
  }

  async setPdfSettings(inspeccion: Inspeccion, position?: number) {
    const videoSections: { text: string }[] = [];
    const audioSections: { text: string }[] = [];
    const fotoSections: { image: string, width: string }[] = [];
    const estados: { text: string, background?: string, color?: string, margin?: number[], bold?: boolean }[] = [];
    let disciplinas, disciplinasText;

    if (inspeccion.documentos.videos) {
      inspeccion.documentos.videos.forEach((videoUrl, index) => {
        videoSections.push(
          { text: videoUrl },
        );
      });
    }

    if (inspeccion.documentos.audios) {
      inspeccion.documentos.audios.forEach((audioUrl, index) => {
        audioSections.push(
          { text: audioUrl },
        );
      });
    }

    let fechaCreacionString = inspeccion.fechaCreacion.toString();
    let fechaCreacion = this.convertDate(fechaCreacionString);

    estados.push(
      { text: inspeccion.estado, background: inspeccion.estado === 'conforme' ? 'green' : 'red', color: 'white' },
    );

    if (inspeccion.documentos.fotos.length > 0) {
      this.inspeccionUrlPics.forEach((foto, index) => {
        let width = 200;

        if (inspeccion.documentos.fotos.length > 1) {
          width = 150;
        }

        fotoSections.push(
          { image: foto, width: `${width}` },
        );
      });
    }

    return {
      videoSections: videoSections,
      audioSections: audioSections,
      fotoSections: fotoSections,
      estados: estados,
      fechaCreacion: fechaCreacion,
    };

  }

  setBody(inspeccion: Inspeccion, objectData: any) {
    return [
      [{ text: "Código de proyecto", alignment: "left", fillColor: '#f2f2f2' }, { text: inspeccion.proyectoCod }],
      [{ text: "Código de incidencia", alignment: "left", fillColor: '#f2f2f2' }, { text: inspeccion.incidenciaCod }],
      [{ text: "Código de inspección", alignment: "left", fillColor: '#f2f2f2' }, { text: inspeccion.codigo }],
      [{ text: "Usuario", alignment: "left", fillColor: '#f2f2f2' }, { text: inspeccion.autorNombre }],
      [{ text: "Fecha de creación", alignment: "left", fillColor: '#f2f2f2' }, { text: objectData.fechaCreacion }],
      [{ text: "Descripción", alignment: "left", fillColor: '#f2f2f2' }, { text: inspeccion.descripcion }],
      [{ text: "Complemento 1", alignment: "left", fillColor: '#f2f2f2' }, { text: inspeccion.complemento1 }],
      [{ text: "Complemento 2", alignment: "left", fillColor: '#f2f2f2' }, { text: inspeccion.complemento2 }],
      [{ text: "Foto", alignment: "left", fillColor: '#f2f2f2' },
        {
          stack: [
            objectData.fotoSections
          ]
        }],

      [{ text: "Audio", alignment: "left", fillColor: '#f2f2f2' },
        {
          stack: [
            objectData.audioSections
          ]
        }],
      [{ text: "Vídeo", alignment: "left", fillColor: '#f2f2f2' },
        {
          stack: [
            objectData.videoSections
          ]
        }],
      [{ text: "Estado", alignment: "left", fillColor: '#f2f2f2' }, { text: objectData.estados,  background: 'red' }],
    ];
  }

  setPageContent(body: any[]){
    return [
      {
        text: `Informe de inspección`,
        margin: [0, 20, 0, 10],
        fontSize: 16,
        bold: true
      },
      {
        type: "none",
        fontSize: 12,
        margin: [0, 15, 5, 0],
        ol: [
          {
            style: "totalsTable",
            table: {
              heights: 10,
              widths: ['auto', '*'],
              body:
                body
              ,
            },
          },
        ],
      },
    ];
  }

  async printPdf(pagesContent: any[]) {
    const pdfDefinition: any = {
      header: () => {
        return [
          {
            table: {

              widths: ['*'],
              body: [[
                {
                  border: [false, false, false, false],
                  fillColor: '#00356C',
                  image: this.logoText,
                  width: 150
                },
              ]]
            },
          },
          { canvas: [{ type: 'rect', x: 170, y: 0, w: 180, h: 10 }] }
        ]
      },
      content: [
        ...pagesContent
      ],
      // pageMargins: [10, 20, 10, 10]
    }

    const pdf = pdfMake.createPdf(pdfDefinition);
    pdf.open();
  }

  async getUsuariosEspecialidad(especialidad: string): Promise<Usuario[]> {
    return new Promise((resolve, reject) => {
      this.usuarioService.getUsuariosEspecialidad(especialidad)
      .subscribe({
        next: res => {
          resolve(res.data);
        },
        error: error => {
          reject(error);
          Swal.fire({icon: 'error', title: 'Oops...', text: 'No se pudo mandar notificación de la inspección',});
        }
      });
    })
  }

  abrirAviso(inspeccion: Inspeccion) {
    this.showElement(true, this.avisoInspeccion.nativeElement);
    this.notificationData = inspeccion;
  }

  cancelNotify() {
    this.showElement(false, this.avisoInspeccion.nativeElement)
  }

  deleteInspeccion(inspeccion: Inspeccion){
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-secondary me-4'
      },
      buttonsStyling: false
    })
    swalWithBootstrapButtons.fire({
      title: `Eliminar inspeccion`,
      text: `Se va a eliminar la inspeccion ${inspeccion.codigo}. ¿Desea continuar?`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Aceptar',
      cancelButtonText: 'Cancelar',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        this.deleteIncFromEverywhere(inspeccion);
      }
    });
  }

  async deleteIncFromEverywhere(inspeccion: Inspeccion){
    const { fotos, audios, videos } = inspeccion.documentos;

    const procesarArchivos = async (archivos: string[]) => {
      for(const archivo of archivos){
        await this.deleteFilesFromAWS(archivo);
      }
    };

    if(fotos.length > 0){
      await procesarArchivos(fotos);
    }

    if(audios.length > 0){
      await procesarArchivos(audios);
    }

    if(videos.length > 0){
      await procesarArchivos(videos);
    }


    this.inspeccionService.deleteInspeccion(inspeccion.uid)
    .subscribe({
      next: res => {
        this.successMessage();
        this.getInitialInfo();
      },
      error: error => {
        Swal.fire({ icon: 'error', title: 'Oops...', text: 'No se pudo eliminar el archivo de AWS', });
      }
    });
  }

  deleteFilesFromAWS(object: string){
    this.mediaService.deleteFile(object)
    .subscribe({
      next: res => {
      },
      error: error => {
        Swal.fire({ icon: 'error', title: 'Oops...', text: `No se pudo eliminar el archivo ${object} de AWS`, });
      }
    });
  }

  successMessage() {
    let message = 'La inspección se ha eliminado con éxito';

    Swal.fire({
      position: 'center',
      icon: 'success',
      title: message,
      showConfirmButton: false,
      timer: 1500
    });
  }


  showElement(hide: boolean, elemento: HTMLDivElement, property?: string){
    let hidenClass = 'visibility-hidden';
    let shownClass = 'visibility-visible';

    if(property){
      hidenClass = 'display-none';
      shownClass = 'display-block';
    }

    if(hide){
      this.renderer2.removeClass(elemento, hidenClass);
      this.renderer2.addClass(elemento, shownClass);
    }
    else{
      this.renderer2.removeClass(elemento, shownClass);
      this.renderer2.addClass(elemento, hidenClass);
    }
  }

  ordenar(orden: string, tipo: string){
    this.orden = orden;
    this.tipo = tipo;
    this.getInspecciones();
  }

  convertDate(date: string): string{
    let fullDate = date.split("T");
    let day = fullDate[0].split("-");
    let time = fullDate[1].split(":");
    let seconds = time[2].split('.')[0];

    let newDate = `${day[2]}-${day[1]}-${day[0]}   ${time[0]}:${time[1]}:${seconds}`;

    return newDate;
  }

  removeDate(){
    this.calendar.nativeElement.reset();
    this.showElement(false, this.calendar.nativeElement);

    this.startDate.nativeElement.value = '';
    this.endDate.nativeElement.value = '';
    this.startDateCalendar =  '';
    this.endDateCalendar = '';

    this.getInitialInfo();
  }

  searchDate(){
    this.startDateCalendar =  this.startDate.nativeElement.value;
    this.endDateCalendar = this.endDate.nativeElement.value;
    this.showElement(false, this.calendar.nativeElement);

    this.getInitialInfo();
  }

  openCalendar(){
    this.showElement(true, this.calendar.nativeElement);
  }

  closeCalendar(){
    this.showElement(false, this.calendar.nativeElement);
  }

}
