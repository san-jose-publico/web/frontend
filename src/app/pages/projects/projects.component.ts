import { Component, ElementRef, EventEmitter, Input, OnInit, Output, Renderer2, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Incidencia } from 'src/app/interfaces/Incidencia';
import { Inspeccion } from 'src/app/interfaces/Inspeccion';
import { Proyecto } from 'src/app/interfaces/Proyecto';
import { Usuario } from 'src/app/interfaces/Usuario';
import { AuthService } from 'src/app/services/auth.service';
import { IncidenciaService } from 'src/app/services/incidencia.service';
import { InspeccionService } from 'src/app/services/inspeccion.service';
import { MediaService } from 'src/app/services/media.service';
import { ProyectoService } from 'src/app/services/proyecto.service';
import { UsuarioService } from 'src/app/services/user.service';
import { environment } from 'src/environments/env';
import Swal from 'sweetalert2';

type MensajeAccion = 'crear' | 'eliminar' | 'editar' | 'activar';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.css']
})
export class ProjectsComponent  implements OnInit{
  proyectoForm: FormGroup;
  proyectoCopia!: Proyecto;
  crear: boolean = false;
  files: File[] = [];
  proyectos: Proyecto[] = [];
  incidenciasProyecto: Incidencia[] = [];
  inspeccionesProyecto: Inspeccion[] = [];
  incidenciasT: Incidencia[] = [];
  usuarios: Usuario[] = [];
  proyectoId: string = '';
  mostrarModal: boolean = false;
  orden: string = 'desc';
  tipo: string = 'fechaCreacion';
  qrContenido: string = '';
  showQR: boolean = false;
  loadCompleted: boolean = false;
  //pagination
  recordsTotal: number = 0;
  recordCurrent: number = 0;
  recordsPerPage: number = environment.recordsPerPage;

  accionesMensajes: Record<MensajeAccion, string> = {
    crear: 'El proyecto se ha creado con éxito',
    eliminar: 'El proyecto se ha eliminado con éxito',
    editar: 'El proyecto se ha actualizado con éxito',
    activar: 'El proyecto se ha activado con éxito',
  };

  //Busqueda
  searchResults: string[] = [];
  filterField: string = '';
  filterWord: string = '';
  startDateCalendar: string = '';
  endDateCalendar: string = '';
  filters: Map<string,string> = new Map([
    ['Código', 'codigo'],
    ['Título', 'nombre'],
    ['Usuario asignado', 'autor'],
    ['Documentación', 'documentos'],
  ]);

  @ViewChild('modal') modal!: ElementRef;
  @ViewChild('documentacion') documentacion!: ElementRef;
  @ViewChild('incidencias') incidencias!: ElementRef;
  @ViewChild('copia') copia!: ElementRef;
  @ViewChild('inputIncidencia') inputIncidencia!: ElementRef;
  @ViewChild('documentacionLista') documentacionLista!: ElementRef;
  @ViewChild('botonDocu') botonDocu!: ElementRef;
  @ViewChild('incidenciasLista') incidenciasLista!: ElementRef;
  @ViewChild('botonInci') botonInci!: ElementRef;
  @ViewChild('documentosForm') documentosForm!: ElementRef;

  @ViewChild('searchInput') searchInput!: ElementRef;
  @ViewChild('calendar') calendar!: ElementRef;
  @ViewChild('startDate') startDate!: ElementRef;
  @ViewChild('endDate') endDate!: ElementRef;

  @Input() hideModal!: string;


  ngOnInit(): void {
    this.getInitialInfo();
  }

  constructor(
    private fb: FormBuilder,
    private renderer2: Renderer2,
    private router: Router,
    private proyectoService: ProyectoService,
    private incidenciaService: IncidenciaService,
    private inspeccionService: InspeccionService,
    private usuarioService: UsuarioService,
    private mediaService: MediaService,
    private authService: AuthService
  ){
    this.proyectoForm = this.fb.group({
      nombre: ['', Validators.required],
      documentos: ['', Validators.required],
      autor: ['', Validators.required],
      autorNombre: ['', Validators.required],
    });
  }

  getInitialInfo(){
    this.getProyectos();
    this.getIncidencias();
    this.getUsuarios();
  }

  //Paginacion
  changePage(page: number){
    page = (page < 0 ? 0 : page);
    this.recordCurrent = ((page - 1) * this.recordsPerPage >=0 ? (page - 1) * this.recordsPerPage : 0);
    this.getInitialInfo();
  }

  search(event: Event) {
    const query = (event.target as HTMLInputElement).value;
    const results = [];

    if (query.length > 0) {
        results.push(`<strong>Código:</strong> ${query}`);
        results.push(`<strong>Título:</strong> ${query}`);
        results.push(`<strong>Usuario asignado:</strong> ${query}`);
        results.push(`<strong>Documentación:</strong> ${query}`);
    }
    else{
      this.filterField = '';
      this.filterWord = '';
      this.getInitialInfo();
    }
    this.searchResults = results;
  }

  async selectResult(result: string) {
    const regex = /<strong>(.*?)<\/strong>/;
    const matches = result.match(regex)!;
    let category = matches[1].replace(':', '');
    let query = this.searchInput.nativeElement.value;

    if(category === 'Usuario asignado'){
      await this.getUsuarios();
      query = this.getIdElement(query, this.usuarios);
    }

    let filtro: string | undefined;
    filtro = this.filters.get(category);

    this.filterField =  filtro!;
    this.filterWord = query;

    this.searchResults = [];
    this.recordCurrent = 0;
    this.getInitialInfo();
  }

  getIdElement(nombre: string, arrayList: Array<Usuario>): string[] | string {
    const normalizedName = nombre.normalize("NFD").replace(/[\u0300-\u036f]/g, "").toLowerCase();
    const words = normalizedName.split(' ');
    const regex = new RegExp(`(${this.escapeRegExp(words.join('|'))})`, "i");
    let idArray;

    idArray = arrayList
      .filter((usuario) => {
        const normalizedName = this.normalizeWord(usuario.nombre);
        const normalizedUsername = this.normalizeWord(usuario.apellidos);
        return regex.test(normalizedName) || regex.test(normalizedUsername);
      })
      .map((element) => element.uid);

      return idArray.length > 0 ? idArray : '';
    }

  normalizeWord(word: string): string{
    return word.normalize("NFD").replace(/[\u0300-\u036f]/g, "").toLowerCase();
  }

  escapeRegExp(string: string): string{
    return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
  }

  descargar(name: string){
    this.mediaService.getFile(name)
    .subscribe({
      next: async res => {
        const link = document.createElement('a');
        link.href = res;
        link.target = '_blank';
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
      },
      error: error => {
        Swal.fire({icon: 'error', title: 'Oops...', text: 'No se pudo descargar el archivo',});
      }
    });
  }

  esconderModal(){
    if(this.crear){
      this.crear = false;
    }

    this.documentacion.nativeElement.innerHTML = '';
    this.files = [];
    this.showElement(false, this.modal.nativeElement);
  }

  crearProyecto(){
    let fileNames = [];
    for(let file of this.files){
      fileNames.push(file.name)
    }

    let nombre = '';
    this.authService.getName().subscribe(name => {
      nombre = name;
    });
    this.proyectoForm.patchValue({
      autorNombre: nombre,
      autor: this.authService.getUid(),
      documentos: fileNames
    });

    this.proyectoService.crearProyecto(this.proyectoForm.value)
    .subscribe({
      next: res => {
        const formData = new FormData();
        for(let file of this.files){
          formData.append('files', file);
        }

        this.mediaService.uploadFile(formData)
        .subscribe({
          next: res => {
            this.proyectoForm.markAsPristine();
            this.esconderModal();
            this.getInitialInfo();
            this.documentacion.nativeElement.innerHTML = '';
          },
          error: error => {
            Swal.fire({icon: 'error', title: 'Oops...', text: 'No se pudo subir el archivo'+error});
          }
        });
      },
      error: error => {
        Swal.fire({icon: 'error', title: 'Oops...', text: 'No se pudo crear el proyecto',});
      }
    });
  }

  verProyecto(project: Proyecto){
    this.proyectoCopia = project;
    this.getIncidenciasProyecto();
    this.getInspeccionesProyecto();
    this.showElement(true, this.copia.nativeElement);

  }

  cerrarInfo(){
    this.showElement(false, this.copia.nativeElement);
    this.showElement(false, this.documentacionLista.nativeElement, 'display');
    this.showElement(false, this.incidenciasLista.nativeElement, 'display');
  }

  editarProyecto(){
    this.showElement(true, this.modal.nativeElement);

    this.proyectoForm.get('nombre')?.setValue(this.proyectoCopia.nombre);
    this.proyectoForm.setValue({
      nombre: this.proyectoCopia.nombre,
      documentos: this.proyectoCopia.documentos,
      autor: this.proyectoCopia.autor,
      autorNombre: this.proyectoCopia.autorNombre,
    });
  }


  abrirModalVacio(){
    this.proyectoForm.reset();
    this.crear = true;
    this.showElement(true, this.modal.nativeElement);
  }

  addFile(event: any){
    this.files = event.target.files;
    const reader = new FileReader();
    let url;
    const fileToRead: File = this.files[0];

    reader.onload = (e: any) => {
      url = e.target.result;
    };
    reader.readAsDataURL(fileToRead);


    for(let file of this.files){
      const [div, span] = this.addTag(file.name, this.documentacion.nativeElement);

      let pos = 0;

      this.renderer2.listen(span, 'click', () =>{
        this.renderer2.removeChild(this.documentacion.nativeElement, div);

        const fileListArr = Array.from(this.files)
        fileListArr.forEach((element,index)=>{
          if(element.name === file.name ){
            fileListArr.splice(index,1);
          }
        });

        this.files = [];

        fileListArr.forEach((element,index)=>{
          this.files.push(element);
        });
      });
    }
  }

  // addIncidencia(){
  //   if(this.proyectoForm.get('incidencias')?.value && this.proyectoForm.get('situacion_incidencia')?.value && this.proyectoForm.get('disciplina')?.value){
  //     console.log(this.proyectoForm.value);
  //     const [div, span] = this.addTag(this.proyectoForm.get('incidencias')!.value, this.incidencias.nativeElement);
  //     let pos = 0;

  //     this.renderer2.listen(span, 'click', () =>{
  //       this.renderer2.removeChild(this.incidencias.nativeElement, div);
  //     });

  //     this.proyectoForm.patchValue({
  //       incidencias: '',
  //       situacion_incidencia: '',
  //       disciplina: ''
  //     })

  //   }
  //   else{
  //     this.inputIncidencia.nativeElement.focus();
  //   }

  // }

  addTag(nombre: string, elemento: any){
    const div = this.renderer2.createElement('div');
    const el = this.renderer2.createElement('p');
    const span = this.renderer2.createElement('span');
    const textSpan = this.renderer2.createText('\u2715');
    const text = this.renderer2.createText(nombre);

    this.renderer2.appendChild(el, text);
    this.renderer2.appendChild(span, textSpan);
    this.renderer2.appendChild(el, span);
    this.renderer2.setAttribute(span,'style', 'cursor: pointer; float: right');
    this.renderer2.appendChild(div, el);
    this.renderer2.setAttribute(div,'style', 'display: flex');

    if(elemento === this.documentacion.nativeElement){
      this.renderer2.setAttribute(el,'style', 'width: 100%');
      this.renderer2.addClass(span,'ms-2');
    }
    else{
      this.renderer2.setAttribute(el,'style', 'width: 70px; margin-left: 10px;');
      this.renderer2.addClass(el,'tag');

    }

    this.renderer2.appendChild(elemento, div);

    return [div, span]
  }

  openPdf(url: any){
    // Extraer el contenido base64 de la data URI
    const base64Content = url.split(',')[1];

    // Decodificar el contenido base64 a un array de bytes (Uint8Array)
    const byteCharacters = atob(base64Content);
    const byteArray = new Uint8Array(byteCharacters.length);
    for (let i = 0; i < byteCharacters.length; i++) {
      byteArray[i] = byteCharacters.charCodeAt(i);
    }

    // Crear un Blob a partir del array de bytes
    const blob = new Blob([byteArray], { type: 'application/pdf' });

    // Crear una URL (Object URL) desde el Blob
    const pdf = URL.createObjectURL(blob);

    // Abrir el archivo en una nueva pestaña del navegador
    window.open(pdf);
  }

  verInforme(uid: string){
    this.router.navigate(['informes'], {
      queryParams: {
        proyecto: uid,
      }
    });
  }

  getProyectos(){
    this.proyectoService.getProyectos(this.recordCurrent, this.tipo, this.orden, this.startDateCalendar, this.endDateCalendar, this.filterField, this.filterWord)
    .subscribe({
      next: async res => {
        this.proyectos = res.data;
        this.recordsTotal = res.recordsTotal;
        if(this.proyectos && this.proyectos.length > 0){
          const promises = this.proyectos.map(async (proyecto) => {
            proyecto.autorNombre = await this.getNombreUsuario(proyecto.autor);
          });
          await Promise.all(promises);
        }
        this.loadCompleted = true;
      },
      error: error => {
        Swal.fire({icon: 'error', title: 'Oops...', text: 'No se pudo obtener los proyectos',});
      }
    });
  }

  async getNombreUsuario(uid: string): Promise<string> {
    return new Promise((resolve, reject) => {
      this.usuarioService.getUsuario(uid)
        .subscribe({
          next: res => {
            resolve(res.data.nombre + ' ' + res.data.apellidos);
          },
          error: error => {
            reject(error);
            Swal.fire({ icon: 'error', title: 'Oops...', text: 'No se pudo obtener el nombre de usuario', });
          }
        });
    })
  }

  getIncidencias(){
    this.incidenciaService.getIncidencias()
    .subscribe({
      next: res => {
       this.incidenciasT = res.data;
      },
      error: error => {
        Swal.fire({icon: 'error', title: 'Oops...', text: 'No se pudo obtener las incidencias',});
      }
    });
  }

  getIncidenciasProyecto(){
    this.incidenciaService.getIncidenciasProyecto(this.proyectoCopia.uid)
    .subscribe({
      next: res => {
       this.incidenciasProyecto = res.data;
      },
      error: error => {
        Swal.fire({icon: 'error', title: 'Oops...', text: 'No se pudo obtener las incidencias del proyecto',});
      }
    });
  }

  getInspeccionesProyecto(){
    this.inspeccionService.getInspeccionesProyecto(this.proyectoCopia.uid)
    .subscribe({
      next: res => {
       this.inspeccionesProyecto = res.data;
      },
      error: error => {
        Swal.fire({icon: 'error', title: 'Oops...', text: 'No se pudo obtener las inspecciones del proyecto',});
      }
    });
  }

  async getUsuarios() {
    return new Promise((resolve, reject) => {
      this.usuarioService.getUsuarios()
        .subscribe({
          next: res => {
            this.usuarios = res.data;
            resolve(res);
          },
          error: error => {
            reject(error);
            Swal.fire({ icon: 'error', title: 'Oops...', text: 'No se pudo obtener los usuarios', });
          }
        });
    })
  }
  verInformacion(elemento: HTMLDivElement, boton: HTMLDivElement){
    if(boton.innerHTML === '+'){
      this.showElement(true, elemento, 'display');
      boton.innerHTML = '-';
    }
    else{
      this.showElement(false, elemento, 'display');
      boton.innerHTML = '+';
    }
  }


  async deleteFile(nombre: string){
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-secondary me-4'
      },
      buttonsStyling: false
    })
    swalWithBootstrapButtons.fire({
      title: 'Eliminar archivo',
      text: `Se va a eliminar el archivo ${nombre}. ¿Desea continuar?`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Aceptar',
      cancelButtonText: 'Cancelar',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        this.mediaService.deleteFile(nombre)
        .subscribe({
          next: async res => {
            await this.eliminarDocumentacion(nombre);
          },
          error: error => {
            Swal.fire({icon: 'error', title: 'Oops...', text: 'No se pudo eliminar el archivo',});
          }
        });
      }
    });
  }

  async eliminarDocumentacion(archivo: string){
    //Elimino el archivo dentro del array de documentos del proyecto
    this.proyectoCopia.documentos.forEach((element,index)=>{
      if(element === archivo ){
        this.proyectoCopia.documentos.splice(index,1);
      }
    });

    //Elimino visualmente del DOM
    for (let i = 0; i < this.documentosForm.nativeElement.children.length; i++) {
      const hijo = this.documentosForm.nativeElement.children[i] as HTMLElement;
      if (hijo.innerHTML.includes(archivo)) {
        this.renderer2.removeChild(this.documentosForm.nativeElement, hijo);
      }
    }
  }


  actualizarProyecto(){
    if(this.files.length > 0){
      for(let file of this.files){
        this.proyectoCopia.documentos.push(file.name)
      }

      this.proyectoForm.patchValue({
        documentos:  this.proyectoCopia.documentos
      });


      const formData = new FormData();
      for(let file of this.files){
        formData.append('files', file);
      }

      this.mediaService.uploadFile(formData)
      .subscribe({
        next: res => {

        },
        error: error => {
          Swal.fire({icon: 'error', title: 'Oops...', text: 'No se pudo subir el archivo'+error});
        }
      });
    }

    this.proyectoService.actualizarProyecto(this.proyectoCopia.uid, this.proyectoForm.value)
    .subscribe({
      next: res => {
        this.proyectoForm.markAsPristine();
        this.esconderModal();
        this.getInitialInfo();
        this.documentacion.nativeElement.innerHTML = '';
        this.mensajeExito('editar');
      },
      error: error => {
        Swal.fire({icon: 'error', title: 'Oops...', text: 'No se pudo actualizar el proyecto',});
      }
    });
  }

  async editarDocumentacion(){
    //Guardo los cambios en la base de datos
    const body = {
      documentos: this.proyectoCopia.documentos
    }
    await this.proyectoService.editarDocumentacionProyecto(this.proyectoCopia.uid, body)
    .subscribe({
      next: res => {
        this.mensajeExito('editar');
      },
      error: error => {
        Swal.fire({icon: 'error', title: 'Oops...', text: 'No se pudo eliminar el archivo',});
      }
    });
  }

  openQR(uid: string){
    this.showQR = true;
    this.qrContenido = uid;
  }

  closeQR(){
    this.showQR = false;
  }

  mensajeExito(accion: MensajeAccion) {
    Swal.fire({
      position: 'center',
      icon: 'success',
      title: this.accionesMensajes[accion],
      showConfirmButton: false,
      timer: 1500
    });
  }

  showElement(hide: boolean, elemento: HTMLDivElement, property?: string){
    let hidenClass = 'visibility-hidden';
    let shownClass = 'visibility-visible';

    if(property){
      hidenClass = 'display-none';
      shownClass = 'display-block';
    }

    if(hide){
      this.renderer2.removeClass(elemento, hidenClass);
      this.renderer2.addClass(elemento, shownClass);
    }
    else{
      this.renderer2.removeClass(elemento, shownClass);
      this.renderer2.addClass(elemento, hidenClass);
    }
  }

  crearIncidencia(){
    this.proyectoId = this.proyectoCopia.uid;
    this.mostrarModal = true;
  }

  getHideModal(modal: boolean){
    if(modal){
      this.mostrarModal = false;
    }
  }

  ordenar(orden: string, tipo: string){
    this.orden = orden;
    this.tipo = tipo;
    this.getInitialInfo();
  }

  removeDate(){
    this.calendar.nativeElement.reset();
    this.showElement(false, this.calendar.nativeElement);

    this.startDate.nativeElement.value = '';
    this.endDate.nativeElement.value = '';
    this.startDateCalendar =  '';
    this.endDateCalendar = '';

    this.getInitialInfo();
  }

  searchDate(){
    this.startDateCalendar =  this.startDate.nativeElement.value;
    this.endDateCalendar = this.endDate.nativeElement.value;
    this.showElement(false, this.calendar.nativeElement);

    this.getInitialInfo();
  }

  openCalendar(){
    this.showElement(true, this.calendar.nativeElement);
  }

  closeCalendar(){
    this.showElement(false, this.calendar.nativeElement);
  }

}
