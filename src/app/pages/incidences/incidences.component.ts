import { Component, ElementRef, OnInit, QueryList, Renderer2, ViewChild, ViewChildren } from '@angular/core';
import { Incidencia } from 'src/app/interfaces/Incidencia';
import { IncidenciaService } from 'src/app/services/incidencia.service';
import { MediaService } from 'src/app/services/media.service';
import Swal from 'sweetalert2';
import * as pdfMake from 'pdfmake/build/pdfmake';
import * as pdfFonts from "pdfmake/build/vfs_fonts";
import { HttpClient } from '@angular/common/http';
import { UsuarioService } from 'src/app/services/user.service';
import { ProyectoService } from 'src/app/services/proyecto.service';
import { firstValueFrom } from 'rxjs';
import { EspecialidadService } from 'src/app/services/especialidad.service';
import { Especialidad } from 'src/app/interfaces/Especialidad';
import { Usuario } from 'src/app/interfaces/Usuario';
import { Inspeccion } from 'src/app/interfaces/Inspeccion';
import { Proyecto } from 'src/app/interfaces/Proyecto';
import { environment } from 'src/environments/env';
import { AuthService } from 'src/app/services/auth.service';
import { codeContractService } from 'src/app/services/codeContract.service';
import * as CryptoJS from 'crypto-js';

(<any>pdfMake).vfs = pdfFonts.pdfMake.vfs;

interface InspeccionesData {
  inspecciones: Inspeccion[];
  urlPics: string[][];
}

@Component({
  selector: 'app-incidences',
  templateUrl: './incidences.component.html',
  styleUrls: ['./incidences.component.css']
})
export class IncidencesComponent implements OnInit {
  incidencias: Incidencia[] = [];
  logoText!: string;
  incidenciaAviso!: Incidencia;
  orden: string = 'desc';
  tipo: string = 'fechaCreacion';
  loadCompleted: boolean = false;
  especialidades: Especialidad[] = [];
  inspeccionesData: InspeccionesData = {
    inspecciones: [],
    urlPics: []
  };
  incidenciaUrlPics: string[] = [];
  proyectos: Proyecto[] = [];
  usuarios: Usuario[] = [];
  notificationData!: Incidencia;
  notificationPdf!: FormData;
  blockChainId: string = '';
  incidenciaId: string = '';

  //pagination
  recordsTotal: number = 0;
  recordCurrent: number = 0;
  recordsPerPage: number = environment.recordsPerPage;

  //Busqueda
  searchResults: string[] = [];
  filterField: string = '';
  filterWord: string = '';
  startDateCalendar: string = '';
  endDateCalendar: string = '';
  filters: Map<string,string> = new Map([
    ['Código', 'codigo'],
    ['Cód. proyecto', 'proyecto'],
    ['Nombre proyecto', 'proyecto'],
    ['Usuario', 'autor'],
    ['Descripción', 'descripcion'],
    ['C1', 'complemento1'],
    ['C2', 'complemento2'],
  ]);

  //blockchain
  fileBc!: File;

  @ViewChildren('op') op!: QueryList<any>;
  @ViewChild('avisoIncidencia') avisoIncidencia!: ElementRef;

  @ViewChild('searchInput') searchInput!: ElementRef;
  @ViewChild('calendar') calendar!: ElementRef;
  @ViewChild('startDate') startDate!: ElementRef;
  @ViewChild('endDate') endDate!: ElementRef;

  @ViewChild('blockChainModal') blockChainModal!: ElementRef;
  @ViewChild('inputFileBc') inputFileBc!: ElementRef;

  ngOnInit(): void {
    this.getInitialInfo();
    this.loadImageFromBase64File();
  }

  constructor(
    private incidenciaService: IncidenciaService,
    private proyectoService: ProyectoService,
    private mediaService: MediaService,
    private http: HttpClient,
    private usuarioService: UsuarioService,
    private renderer2: Renderer2,
    private especialidadService: EspecialidadService,
    private authService: AuthService,
    private codeContractService: codeContractService
  ) {
  }

  getInitialInfo(){
    this.getIncidencias();
  }

  //Paginacion
  changePage(page: number){
    page = (page < 0 ? 0 : page);
    this.recordCurrent = ((page - 1) * this.recordsPerPage >=0 ? (page - 1) * this.recordsPerPage : 0);
    this.getInitialInfo();
  }

  search(event: Event) {
    const query = (event.target as HTMLInputElement).value;
    const results = [];

    if (query.length > 0) {
        results.push(`<strong>Código:</strong> ${query}`);
        results.push(`<strong>Cód. proyecto:</strong> ${query}`);
        results.push(`<strong>Nombre proyecto:</strong> ${query}`);
        results.push(`<strong>Usuario:</strong> ${query}`);
        results.push(`<strong>Descripción:</strong> ${query}`);
        results.push(`<strong>C1:</strong> ${query}`);
        results.push(`<strong>C2:</strong> ${query}`);
    }
    else{
      this.filterField = '';
      this.filterWord = '';
      this.getInitialInfo();
    }
    this.searchResults = results;
  }

  async selectResult(result: string) {
    const regex = /<strong>(.*?)<\/strong>/;
    const matches = result.match(regex)!;
    let category = matches[1].replace(':', '');
    let query = this.searchInput.nativeElement.value;

    if(category === 'Nombre proyecto'){
      await this.getProyectos();
      query = this.getIdElement(query, this.proyectos);
    }
    else if(category === 'Usuario'){
      await this.getUsuarios();
      query = this.getIdElement(query, this.usuarios);
    }
    else if(category === 'Cód. proyecto'){
      await this.getProyectos();
      query = this.getIdElement(query, this.proyectos, true);
    }

    let filtro: string | undefined;
    filtro = this.filters.get(category);

    this.filterField =  filtro!;
    this.filterWord = query;

    this.searchResults = [];
    this.recordCurrent = 0;
    this.getInitialInfo();
  }

  getIdElement(nombre: string, arrayList: Array<Proyecto | Usuario>, isCod?: boolean): string[] | string {
    const normalizedName = nombre.normalize("NFD").replace(/[\u0300-\u036f]/g, "").toLowerCase();
    const words = normalizedName.split(' ');
    const regex = new RegExp(`(${this.escapeRegExp(words.join('|'))})`, "i");
    let idArray;

    if('email' in arrayList[0]){
      idArray = arrayList
      .filter((element): element is Usuario => {
        return 'nombre' in element && 'apellidos' in element;
      })
      .filter((usuario) => {
        const normalizedName = this.normalizeWord(usuario.nombre);
        const normalizedUsername = this.normalizeWord(usuario.apellidos);
        return regex.test(normalizedName) || regex.test(normalizedUsername);
      })
      .map((usuario) => usuario.uid);
    }
    else{
      if(isCod){
        idArray = arrayList
        .filter((element): element is Proyecto => {
          return 'numInc' in element;
        })
        .filter((element) => {
          const normalizedName = this.normalizeWord(element.codigo);
          return regex.test(normalizedName);
        })
        .map((element) => element.uid);
      }
      else{
        idArray = arrayList
        .filter((element): element is Proyecto => {
          const normalizedName = this.normalizeWord(element.nombre);
          return regex.test(normalizedName);
        })
        .map((element) => element.uid);
      }
    }

    return idArray.length > 0 ? idArray : '';
  }

  normalizeWord(word: string): string{
    return word.normalize("NFD").replace(/[\u0300-\u036f]/g, "").toLowerCase();
  }

  escapeRegExp(string: string): string{
    return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
  }

  async getProyectos() {
    return new Promise((resolve, reject) => {
      this.proyectoService.getProyectosAll()
        .subscribe({
          next: res => {
            this.proyectos = res.data;
            resolve(res);
          },
          error: error => {
            reject(error);
            Swal.fire({ icon: 'error', title: 'Oops...', text: 'No se pudo obtener los proyectos', });
          }
        });
    })
  }

  async getUsuarios() {
    return new Promise((resolve, reject) => {
      this.usuarioService.getUsuarios()
        .subscribe({
          next: res => {
            this.usuarios = res.data;
            resolve(res);
          },
          error: error => {
            reject(error);
            Swal.fire({ icon: 'error', title: 'Oops...', text: 'No se pudo obtener los usuarios', });
          }
        });
    })
  }

  loadImageFromBase64File() {
    this.http.get('assets/logo.txt', { responseType: 'text' })
      .subscribe(data => {
        this.logoText = data;
      });
  }

  descargar(name: string) {
    this.mediaService.getFile(name)
      .subscribe({
        next: res => {

          fetch(res).then(res => res.blob()).then(file => {
            let tempUrl = URL.createObjectURL(file);
            let aTag = document.createElement('a');
            aTag.href = tempUrl;
            aTag.download = name;
            document.body.appendChild(aTag);
            aTag.click();
            aTag.remove();
          })
        },
        error: error => {
          Swal.fire({ icon: 'error', title: 'Oops...', text: 'No se pudo descargar el archivo', });
        }
      });
  }

  async getEspecialidades() {
    return new Promise((resolve, reject) => {
      this.especialidadService.getEspecialidades()
        .subscribe({
          next: res => {
            this.especialidades = res.data;
            resolve(res);
          },
          error: error => {
            reject(error);
            Swal.fire({ icon: 'error', title: 'Oops...', text: 'No se pudo obtener las especialidades', });
          }
        });
    })
  }

  async getIncidencias() {
    try {
      const res = await firstValueFrom(this.incidenciaService.getIncidencias(this.recordCurrent, this.tipo, this.orden, this.startDateCalendar, this.endDateCalendar, this.filterField, this.filterWord));
      this.incidencias = res.data;
      this.recordsTotal = res.recordsTotal;

      if(this.incidencias && this.incidencias.length > 0){
        const promises = this.incidencias.map(async (incidencia) => {
          [incidencia.proyectoNombre, incidencia.proyectoCod] = await this.getNombreProyecto(incidencia.proyecto);
          incidencia.autorNombre = await this.getNombreUsuario(incidencia.autor);
        });

        await Promise.all(promises);
      }
      this.loadCompleted = true;

    }
    catch (error) {
      Swal.fire({ icon: 'error', title: 'Oops...', text: 'No se pudo obtener las incidencias', });
    }
  }

  async getNombreProyecto(uid: string): Promise<string[]> {
    try {
      const res = await firstValueFrom(this.proyectoService.getProyecto(uid));
      return [res.data.nombre, res.data.codigo];
    } catch (error) {
      Swal.fire({ icon: 'error', title: 'Oops...', text: 'No se pudo obtener el nombre ni el código del proyecto', });
      throw error;
    }
  }

  async getNombreUsuario(uid: string): Promise<string> {
    return new Promise((resolve, reject) => {
      this.usuarioService.getUsuario(uid)
        .subscribe({
          next: res => {
            resolve(res.data.nombre + ' ' + res.data.apellidos);
          },
          error: error => {
            reject(error);
            Swal.fire({ icon: 'error', title: 'Oops...', text: 'No se pudo obtener los nombres de usuarios', });
          }
        });
    })
  }

  async getLink(archivo: string): Promise<string> {
    return new Promise<string>((resolve, reject) => {
      this.mediaService.getFile(archivo)
        .subscribe({
          next: res => {
            resolve(res);
          },
          error: error => {
            reject(error);
          }
        });
    });
  }

  //Creación del pdf de la incidencia
  async createIncidenciaPdf(incidencia: Incidencia, mode?: string){
    let bodies = [];
    let pages = [];
    let page;

    this.incidenciaUrlPics = await this.convertBase64(incidencia);
    console.log(this.incidenciaUrlPics);

    const objectData = await this.setPdfSettings(incidencia);
    const body = await this.setBody(incidencia, objectData);
    bodies.push(body);

    console.log(this.inspeccionesData.inspecciones.length)
    if(mode === 'all'){
      if(this.inspeccionesData.inspecciones.length === 0){
        page = await this.setPageContent(incidencia, body, true);
      }
      else {
        page = await this.setPageContent(incidencia, body);
      }
    }
    else{
      page = await this.setPageContent(incidencia, body, true);
    }

    pages.push(page);
    return [bodies, pages]
  }

  //Creación del pdf de inspecciones
  async createInspeccionesPdf(incidencia: Incidencia){
    let bodies = [];
    let pages = [];

    if (this.inspeccionesData.inspecciones.length > 0) {
      for (let i = 0; i < this.inspeccionesData.inspecciones.length; i++) {
        const inspeccion = this.inspeccionesData.inspecciones[i];
        inspeccion.incidenciaCod = incidencia.codigo;
        inspeccion.proyectoCod = incidencia.proyectoCod;
        this.inspeccionesData.urlPics.push(await this.convertBase64(inspeccion));
        const objectData = await this.setPdfSettings(inspeccion, i);
        const body = this.setBody(inspeccion, objectData);
        bodies.push(body);
      }

      if(bodies.length > 0){
        let i = 0;
        if(pages.length > 0){
          i = 1;
        }
        for(i; i < bodies.length; i++){
          let page;
          let pos = i;

          //Se ha creado la incidencia anteriormente
          if(pages.length > 0){
            pos = i - 1;
          }

          //Comprueba si es la última página, si es así, NO hace un salto de página después
          if(i === bodies.length - 1){
            page = this.setPageContent(this.inspeccionesData.inspecciones[pos], bodies[i], true);
          }
          else{
            page = this.setPageContent(this.inspeccionesData.inspecciones[pos], bodies[i]);
          }

          pages.push(page);
        }
      }
    }
    return [bodies, pages]
  }

  //Creación del pdf de cerrada
  async createCerradaPdf(incidencia: Incidencia, mode: string){
    let body = [];
    let pages = [];

    if(incidencia.estado === 'cerrada'){
      let nombre;
      await this.authService.getName().subscribe(name => {
        nombre = name;
      });

      const date = await this.convertDate(incidencia.fechaEstadoCerrada!);

      body.push([{ text: `La incidencia se ha cerrado por ${nombre} a día ${date}`, margin: [0, 20, 0, 10]}]);
      pages.push(body);
    }
    else if(incidencia.estado === 'conforme' && mode === 'cerrada'){
      this.selectStatus('cerrada', incidencia);
    }

    return [body, pages];
  }

  //Generación pdf completo
  async downloadPdf(incidencia: Incidencia, mode?: string, downloadDirectly?: boolean, onlyGenerate?: boolean) {
    this.inspeccionesData.inspecciones = [];
    this.inspeccionesData.urlPics = [];
    let pages: any[] = [];
    let bodies: any[] = [];

    if(!mode){
      return;
    }

    this.inspeccionesData.inspecciones.push(...await this.getInspeccionesFromIncidencia(incidencia.uid));
    const promises = this.inspeccionesData.inspecciones.map(async (inspeccion) => {
      inspeccion.autorNombre = await this.getNombreUsuario(inspeccion.autor);
    });
    await Promise.all(promises);

    //Imprimo incidencias
    if((mode === 'incidencia' && incidencia.estado === 'creada')){
      [bodies, pages] = await this.createIncidenciaPdf(incidencia, mode);
    }
    //O imprimo inspecciones
    else if((mode === 'inspecciones' && incidencia.estado === 'conforme')){
      const [newBodies, newPages] = await this.createInspeccionesPdf(incidencia);
      bodies.push(...newBodies);
      pages.push(...newPages);
    }
    //O imprimo cerrada
    else if(mode === 'cerrada'){
      [bodies, pages] = await this.createCerradaPdf(incidencia, mode);
    }
    //O imprimo todo
    else if(mode === 'all'){
      console.log('entro');
      [bodies, pages] = await this.createIncidenciaPdf(incidencia, mode);
      console.log({bodies}, {pages});

      const [newBodies, newPages] = await this.createInspeccionesPdf(incidencia);
      bodies.push(...newBodies);
      pages.push(...newPages);
      console.log({bodies}, {pages});

      const [nBodies, nPages] = await this.createCerradaPdf(incidencia, mode);
      bodies.push(...nBodies);
      pages.push(...nPages);
      console.log({bodies}, {pages});

    }

    console.log('llego');
    //Evitar imprimir en blanco
    if(pages.length > 0){
      if(downloadDirectly){
        this.printPdf(pages, incidencia.codigo, downloadDirectly);
      }
      else{
        if(onlyGenerate){
          this.printPdf(pages, incidencia.codigo, false, true);
        }
        else{
          this.printPdf(pages, incidencia.codigo);
        }
      }
    }
  }

  //Devuelve las imagenes en base64
  async convertBase64(data: Incidencia | Inspeccion): Promise<string[]> {
    const fotosBase64: string[] = [];

    if (data.documentos.fotos) {
      const fotos = await Promise.all(
        data.documentos.fotos.map(async (foto) => await this.getLink(foto))
      );

      await Promise.all(
        fotos.map(async (foto) => {
          try {
            const dataURL = await new Promise<string>((resolve, reject) => {
              const img = new Image();
              img.src = foto;
              img.crossOrigin = 'Anonymous';

              img.onload = () => {
                const canvas = document.createElement('canvas');
                canvas.width = img.width;
                canvas.height = img.height;

                const ctx = canvas.getContext('2d')!;
                ctx.drawImage(img, 0, 0);

                const dataURL = canvas.toDataURL('image/png');
                resolve(dataURL);
              };

              img.onerror = (error) => {
                reject(error);
              };
            });

            fotosBase64.push(dataURL);
          }
          catch (error) {
            console.error('Error generando imagen:', error);
          }
        })
      );
    }

    console.log(fotosBase64);
    return fotosBase64;
  }

  //Setea la configuración del pdf según si es incidencia o inspección
  async setPdfSettings(data: Incidencia | Inspeccion, position?: number) {
    const videoSections: { text: string }[] = [];
    const audioSections: { text: string }[] = [];
    const fotoSections: { image: string, width: string }[] = [];
    const estados: { text: string, background?: string, color?: string, margin?: number[], bold?: boolean }[] = [];
    let disciplinas, disciplinasText;

    if (data.documentos.videos) {
      data.documentos.videos.forEach((videoUrl, index) => {
        videoSections.push(
          { text: videoUrl },
        );
      });
    }

    if (data.documentos.audios) {
      data.documentos.audios.forEach((audioUrl, index) => {
        audioSections.push(
          { text: audioUrl },
        );
      });
    }

    const fechaCreacion = await this.convertDate(data.fechaCreacion);

    if ("disciplinas" in data) {
      disciplinas = data.disciplinas ? data.disciplinas.map(disciplina => ({ text: disciplina })) : [];
      disciplinasText = disciplinas.map(disciplina => disciplina.text).join(', ');

      let background;

      if (data.estado === 'creada') {
        background = 'blue';
      }
      else if (data.estado === 'conforme') {
        background = '#ff9100';
      }
      else if (data.estado === 'no conforme') {
        background = 'red';
      }
      else {
        background = 'green'
      }
      estados.push(
        { text: data.estado, background: background, color: 'white' },
      );

      if (data.estado === 'conforme' || data.estado === 'conforme' || data.estado === 'cerrada' ) {
        estados.push(
          { text: 'Inspecciones:', margin: [0, 5, 0, 0], bold: true },
        );

        if (this.inspeccionesData.inspecciones.length > 0) {
          for (let inspeccion of this.inspeccionesData.inspecciones) {
            const fechaEstado = await this.convertDate(inspeccion.fechaCreacion);
            estados.push(
              { text: `${inspeccion.codigo} (${fechaEstado})` },
            );
          }
        }
      }

      if (data.documentos.fotos.length > 0) {
        this.incidenciaUrlPics.forEach((foto, index) => {
          let width = 200;

          if (data.documentos.fotos.length > 2) {
            width = 150;
          }

          fotoSections.push(
            { image: foto, width: `${width}` },
          );
        });
      }

      return {
        videoSections: videoSections,
        audioSections: audioSections,
        fotoSections: fotoSections,
        estados: estados,
        disciplinasText: disciplinasText,
        fechaCreacion: fechaCreacion,
      };

    }
    else {
      estados.push(
        { text: data.estado, background: data.estado === 'conforme' ? 'green' : 'red', color: 'white' },
      );

      if (data.documentos.fotos.length > 0) {
        this.inspeccionesData.urlPics[position!].forEach((foto, index) => {
          let width = 200;

          if (data.documentos.fotos.length > 1) {
            width = 150;
          }

          fotoSections.push(
            { image: foto, width: `${width}` },
          );
        });
      }

      return {
        videoSections: videoSections,
        audioSections: audioSections,
        fotoSections: fotoSections,
        estados: estados,
        fechaCreacion: fechaCreacion,
      };
    }
  }


  //Setea el body del pdf
  setBody(data: Incidencia | Inspeccion, objectData: any) {
    if ("disciplinas" in data) {
      return [
        [{ text: "Código de proyecto", alignment: "left", fillColor: '#f2f2f2' }, { text: data.proyectoCod }],
        [{ text: "Código de incidencia", alignment: "left", fillColor: '#f2f2f2' }, { text: data.codigo }],
        [{ text: "Nombre de proyecto", alignment: "left", fillColor: '#f2f2f2' }, { text: data.proyectoNombre }],
        [{ text: "Usuario", alignment: "left", fillColor: '#f2f2f2' }, { text: data.autorNombre }],
        [{ text: "Fecha de creación", alignment: "left", fillColor: '#f2f2f2' }, { text: objectData.fechaCreacion }],
        [{ text: "Descripción", alignment: "left", fillColor: '#f2f2f2' }, { text: data.descripcion }],
        [{ text: "Complemento 1", alignment: "left", fillColor: '#f2f2f2' }, { text: data.complemento1 }],
        [{ text: "Complemento 2", alignment: "left", fillColor: '#f2f2f2' }, { text: data.complemento2 }],
        [{ text: "Disciplinas", alignment: "left", fillColor: '#f2f2f2' }, { text: objectData.disciplinasText }],
        [{ text: "Foto", alignment: "left", fillColor: '#f2f2f2' },
        {
          stack: [
            objectData.fotoSections
          ]
        }],
        [{ text: "Audio", alignment: "left", fillColor: '#f2f2f2' },
          {
            stack: [
              objectData.audioSections
            ]
        }],
        [{ text: "Vídeo", alignment: "left", fillColor: '#f2f2f2' },
          {
            stack: [
              objectData.videoSections
            ]
        }],
        [{ text: "Estado", alignment: "left", fillColor: '#f2f2f2' },
          {
            stack: [
              objectData.estados
            ]
        }],
      ];
    }
    else {
      return [
        [{ text: "Código de proyecto", alignment: "left", fillColor: '#f2f2f2' }, { text: data.proyectoCod }],
        [{ text: "Código de incidencia", alignment: "left", fillColor: '#f2f2f2' }, { text: data.incidenciaCod }],
        [{ text: "Código de inspección", alignment: "left", fillColor: '#f2f2f2' }, { text: data.codigo }],
        [{ text: "Usuario", alignment: "left", fillColor: '#f2f2f2' }, { text: data.autorNombre }],
        [{ text: "Fecha de creación", alignment: "left", fillColor: '#f2f2f2' }, { text: objectData.fechaCreacion }],
        [{ text: "Descripción", alignment: "left", fillColor: '#f2f2f2' }, { text: data.descripcion }],
        [{ text: "Complemento 1", alignment: "left", fillColor: '#f2f2f2' }, { text: data.complemento1 }],
        [{ text: "Complemento 2", alignment: "left", fillColor: '#f2f2f2' }, { text: data.complemento2 }],
        [{ text: "Foto", alignment: "left", fillColor: '#f2f2f2' },
          {
            stack: [
              objectData.fotoSections
            ]
          }],

        [{ text: "Audio", alignment: "left", fillColor: '#f2f2f2' },
          {
            stack: [
              objectData.audioSections
            ]
          }],
        [{ text: "Vídeo", alignment: "left", fillColor: '#f2f2f2' },
          {
            stack: [
              objectData.videoSections
            ]
          }],
        [{ text: "Estado", alignment: "left", fillColor: '#f2f2f2' }, { text: objectData.estados,  background: 'red' }],
      ];
    }
  }

  //Setea el contenido de la página según sea incidencia o inspección
  setPageContent(data: Incidencia | Inspeccion, body: any[], endContent?: boolean){
    const title = "disciplinas" in data ? 'incidencia' : 'inspeccion';
    return [
      {
        text: `Informe de ${title}`,
        margin: [0, 20, 0, 10],
        fontSize: 16,
        bold: true
      },
      {
        type: "none",
        fontSize: 12,
        margin: [0, 15, 5, 0],
        ol: [
          {
            style: "totalsTable",
            table: {
              heights: 10,
              widths: ['auto', '*'],
              body:
                body
              ,
            },
          },
        ],
      },
      endContent ? '' : { text: "", pageBreak: "after" }
    ];
  }

  //Imprime el pdf con los datos seteados
  async printPdf(pagesContent: any[], name: string, downloadDirectly?: boolean, onlyGenerate?: boolean) {
    const pdfDefinition: any = {
      header: () => {
        return [
          {
            table: {

              widths: ['*'],
              body: [[
                {
                  border: [false, false, false, false],
                  fillColor: '#00356C',
                  image: this.logoText,
                  width: 150
                },
              ]]
            },
          },
          { canvas: [{ type: 'rect', x: 170, y: 0, w: 180, h: 10 }] }
        ]
      },
      content: [
        ...pagesContent
      ],
    }

    const pdfName = `${name}_completa.pdf`;
    const pdf = pdfMake.createPdf(pdfDefinition);

    if(onlyGenerate){
      pdf.getBlob((blob) => {
        this.notificationPdf = new FormData();
        this.notificationPdf.append('file', blob, pdfName);
      });
      return;
    }

    if(downloadDirectly){
      pdf.download(pdfName);
      return;
    }

    pdf.open();
  }

  async getInspeccionesFromIncidencia(uid: string): Promise<Inspeccion[]> {
    return new Promise((resolve, reject) => {
      this.incidenciaService.getInspeccionesFromIncidencia(uid)
        .subscribe({
          next: res => {
            resolve(res.data);
          },
          error: error => {
            reject(error);
            Swal.fire({ icon: 'error', title: 'Oops...', text: 'No se pudo obtener las inspecciones de la incidencia', });
          }
        });
    })
  }

  deleteIncidencia(incidencia: Incidencia){
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-secondary me-4'
      },
      buttonsStyling: false
    })
    swalWithBootstrapButtons.fire({
      title: `Eliminar incidencia`,
      text: `Se va a eliminar la incidencia ${incidencia.codigo}. ¿Desea continuar?`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Aceptar',
      cancelButtonText: 'Cancelar',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        this.deleteIncFromEverywhere(incidencia);
      }
    });
  }

  async deleteIncFromEverywhere(incidencia: Incidencia){
    const { fotos, audios, videos } = incidencia.documentos;

    const procesarArchivos = async (archivos: string[]) => {
      for(const archivo of archivos){
        await this.deleteFilesFromAWS(archivo);
      }
    };

    //Elimino los archivos de la incidecia de AWS
    if(fotos.length > 0){
      await procesarArchivos(fotos);
    }

    if(audios.length > 0){
      await procesarArchivos(audios);
    }

    if(videos.length > 0){
      await procesarArchivos(videos);
    }

    //Elimino los archivos de las inspecciones de la incidencia de AWS
    const inspecciones: Inspeccion[]  = await this.getInspeccionesFromIncidencia(incidencia.uid);

    if(inspecciones.length > 0){
      for(const inspeccion of inspecciones){
        const { fotos, audios, videos } = inspeccion.documentos;

        if(fotos.length > 0) {
          await procesarArchivos(fotos);
        }

        if(audios.length > 0) {
          await procesarArchivos(audios);
        }

        if(videos.length > 0) {
          await procesarArchivos(videos);
        }
      }
    }

    //Elimino la incidencia de la db, en esta petición también se eliminan las inspecciones de la db
    this.incidenciaService.deleteIncidencia(incidencia.uid)
    .subscribe({
      next: res => {
        this.successMessage();
        this.getInitialInfo();
      },
      error: error => {
        Swal.fire({ icon: 'error', title: 'Oops...', text: 'No se pudo eliminar la incidencia de AWS', });
      }
    });
  }

  deleteFilesFromAWS(object: string){
    this.mediaService.deleteFile(object)
    .subscribe({
      next: res => {
      },
      error: error => {
        Swal.fire({ icon: 'error', title: 'Oops...', text: `No se pudo eliminar el archivo ${object} de AWS`, });
      }
    });
  }

  successMessage() {
    let message = 'La incidencia se ha eliminado con éxito';

    Swal.fire({
      position: 'center',
      icon: 'success',
      title: message,
      showConfirmButton: false,
      timer: 1500
    });
  }

  async abrirAviso(incidencia: Incidencia) {
    this.notificationData = incidencia;
    // await this.downloadPdf(incidencia, 'all', false, true);
    this.showElement(true, this.avisoIncidencia.nativeElement);
  }

  cancelNotify() {
    this.showElement(false, this.avisoIncidencia.nativeElement);
  }

  showElement(hide: boolean, elemento: HTMLDivElement, property?: string) {
    let hidenClass = 'visibility-hidden';
    let shownClass = 'visibility-visible';

    if (property) {
      hidenClass = 'display-none';
      shownClass = 'display-block';
    }

    if (hide) {
      this.renderer2.removeClass(elemento, hidenClass);
      this.renderer2.addClass(elemento, shownClass);
    }
    else {
      this.renderer2.removeClass(elemento, shownClass);
      this.renderer2.addClass(elemento, hidenClass);
    }
  }

  ordenar(orden: string, tipo: string) {
    this.orden = orden;
    this.tipo = tipo;
    this.getIncidencias();
  }

  convertDate(date: Date): Promise<string> {
    return new Promise(async (resolve, reject) => {
      try{
        const utcDate = new Date(date);
        const spanishDate = utcDate.toLocaleString();

        resolve(spanishDate);
      }catch(error){
        reject(error);
      }
    });
  }

  selectStatus(status: string, incidencia: Incidencia) {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-secondary me-4'
      },
      buttonsStyling: false
    })
    swalWithBootstrapButtons.fire({
      title: `Cambiar estado`,
      text: `Se va a cambiar el estado a cerrada. ¿Desea continuar?`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Aceptar',
      cancelButtonText: 'Cancelar',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        incidencia.estado = status;

        const body = {
          estado: status
        }
        this.incidenciaService.updateStatus(incidencia.uid, body)
          .subscribe({
            next: () => {
              this.getInitialInfo();
            },
            error: error => {
              Swal.fire({ icon: 'error', title: 'Oops...', text: 'No se pudo cambiar el estado, vuelva a intentarlo', });
            }
          });
      }
    });
  }

  removeDate(){
    this.calendar.nativeElement.reset();
    this.showElement(false, this.calendar.nativeElement);

    this.startDate.nativeElement.value = '';
    this.endDate.nativeElement.value = '';
    this.startDateCalendar =  '';
    this.endDateCalendar = '';

    this.getInitialInfo();
  }

  searchDate(){
    this.startDateCalendar =  this.startDate.nativeElement.value;
    this.endDateCalendar = this.endDate.nativeElement.value;
    this.showElement(false, this.calendar.nativeElement);

    this.getInitialInfo();
  }

  openCalendar(){
    this.showElement(true, this.calendar.nativeElement);
  }

  closeCalendar(){
    this.showElement(false, this.calendar.nativeElement);
  }

  addFileBlockChain(event: any){
    this.fileBc = event.target.files[0];
  }

  openBlockChainModal(action: boolean, incidenciaId?: string){
    if(incidenciaId){
      this.incidenciaId = incidenciaId;
    }

    this.inputFileBc.nativeElement.value = '';
    this.showElement(action, this.blockChainModal.nativeElement);
  }

  //proceso completo de la encriptación y el envío del pdf a code contract
  async sendToCodeContract(){
    try {
      const tokenLogin = await this.loginCodeContract();
      const pdfEncrypted = await this.generateHash();
      this.blockChainId  = await this.registerCodeContract(tokenLogin, pdfEncrypted);
      if(this.blockChainId){
        this.setBlockChainId();
        const descarga = await this.downloadCodeContract(tokenLogin, this.blockChainId);
        this.openBlockChainModal(false);
        this.getInitialInfo();
      }
    } catch (error) {
      console.error('Error en el proceso sendToCodeContract:', error);
      Swal.fire({ icon: 'error', title: 'Oops...', text: 'Ocurrió un error en el proceso de envío a Code Contract' });
    }
  }

  setBlockChainId(){
    const body = {
      blockChainId: this.blockChainId
    }

    this.incidenciaService.setBlockChainId(this.incidenciaId, this.blockChainId, body)
    .subscribe({
      next: res => {
      },
      error: error => {
        console.error('No se pudo setear el blockChainId, ', error);
      }
    });
  }

  loginCodeContract(): Promise<string>{
    return new Promise<string>((resolve, reject) => {
      this.codeContractService.login()
      .subscribe({
        next: async res => {
          resolve(res.data.token);
        },
        error: error => {
          console.error('Error haciendo login en Code Contract:', error);
          reject(error);
        }
      });
    });
  }

  async registerCodeContract(token: string, content: string): Promise<string> {
    return new Promise<string>((resolve, reject) => {
      const idProject = '6625424b6a25fab563cc267e';

      const body = {
          "item_hashes_and_metadata": [
              {
                  "item_hash": content,
                  "metadata": [
                      {
                          "key": "Documento",
                          "value": this.fileBc.name,
                      }
                  ]
              }
          ],
          "smart_check_merkle_id": idProject,
      };

      this.codeContractService.registerObject(token, body)
      .subscribe({
        next: res => {
          if(res.code === 'TREE_ALREADY_EXISTS'){
            Swal.fire({ icon: 'error', title: 'Oops...', text: 'El objeto ya está registrado en code contract'});
            reject();
          }
          else{
            const id = res.data.merkle_root_id;
            resolve(id);
          }
        },
        error: error => {
          console.error('No se pudo registrar el objeto en code contract:', error);
          reject(error);
        }
      });
    });
  }

  async downloadZip(blockChainId: string, codigo: string){
    try {
      const tokenLogin = await this.loginCodeContract();
      const zipName = `${codigo}_completa_blockChain.zip`;
      await this.downloadCodeContract(tokenLogin, blockChainId, zipName);
    }
    catch (error) {
      console.error('Error descargando el zip:', error);
      Swal.fire({ icon: 'error', title: 'Oops...', text: 'No se pudo descargar el zip' });
    }
  }

  downloadCodeContract(token: string, merkleRootId: string, zipName?: string): Promise<void>{
    return new Promise<void>((resolve, reject) => {
      const body = {
        "merkle_root_id": merkleRootId
      }

      let name = '';
      if(zipName){
        name = zipName;
      }
      else{
        const fileName = this.fileBc.name.split('.');
        name = fileName[0] + '_blockChain.zip';
      }

      this.codeContractService.downloadObject(token, body, name)
      .subscribe({
        next: async res => {
          resolve(res);
        },
        error: error => {
          console.error('Error descargando el zip:', error);
          reject(error);
        }
      });
    });
  }

  async generateHash(): Promise<string> {
    const fileContent = await this.readFileContent(this.fileBc);
    const byteArray = new Uint8Array(fileContent);
    const hash = CryptoJS.SHA256(this.arrayBufferToHex(byteArray)).toString();

    return hash;
  }

  //Transformo el contenido del pdf en un array buffer con datos binarios para manejar el archivo de forma eficiente
  async readFileContent(file: File): Promise<ArrayBuffer> {
    return new Promise<ArrayBuffer>((resolve, reject) => {
      const reader = new FileReader();
      reader.onload = () => resolve(reader.result as ArrayBuffer);
      reader.onerror = error => reject(error);
      reader.readAsArrayBuffer(file);
    });
  }

  arrayBufferToHex(buffer: Uint8Array): string {
    return Array.prototype.map.call(buffer, (byte: number) => {
      return ('0' + (byte & 0xFF).toString(16)).slice(-2);
    }).join('');
  }

}
