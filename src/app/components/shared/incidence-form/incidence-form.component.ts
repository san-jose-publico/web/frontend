import { Component, ElementRef, EventEmitter, Input, OnInit, Output, Renderer2, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { IncidenciaService } from 'src/app/services/incidencia.service';
import { MediaService } from 'src/app/services/media.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-incidence-form',
  templateUrl: './incidence-form.component.html',
  styleUrls: ['./incidence-form.component.css']
})
export class IncidenceFormComponent implements OnInit{
  @Input() proyecto: string = '';
  incidenciaForm: FormGroup;
  files: File[] = [];
  @Output() hideModal = new EventEmitter<boolean>(false);

  ngOnInit(): void {
  }

  constructor(
    private fb: FormBuilder,
    private renderer2: Renderer2,
    private incidenciaService: IncidenciaService,
    private mediaService: MediaService,
    private authService: AuthService
  ){
    this.incidenciaForm = this.fb.group({
      autor: ['', Validators.required],
      autorNombre: ['', Validators.required],
      descripcion: ['', Validators.required],
      estado: ['', Validators.required],
      proyecto: ['', Validators.required],
      disciplina: ['', Validators.required],
      documentos: ['', Validators.required],
      localizacion: ['', Validators.required],
    });
  }

  @ViewChild('documentacion') documentacion!: ElementRef;


  addFile(event: any){
    this.files = event.target.files;
    const reader = new FileReader();
    let url;
    const fileToRead: File = this.files[0];

    reader.onload = (e: any) => {
      url = e.target.result;
    };
    reader.readAsDataURL(fileToRead);


    for(let file of this.files){
      const [div, span] = this.addTag(file.name, this.documentacion.nativeElement);

      let pos = 0;

      this.renderer2.listen(span, 'click', () =>{
        this.renderer2.removeChild(this.documentacion.nativeElement, div);

        const fileListArr = Array.from(this.files)
        fileListArr.forEach((element,index)=>{
          if(element.name === file.name ){
            fileListArr.splice(index,1);
          }
        });

        this.files = [];

        fileListArr.forEach((element,index)=>{
          this.files.push(element);
        });
      });
    }
  }

  addTag(nombre: string, elemento: any){
    const div = this.renderer2.createElement('div');
    const el = this.renderer2.createElement('p');
    const span = this.renderer2.createElement('span');
    const textSpan = this.renderer2.createText('\u2715');
    const text = this.renderer2.createText(nombre);

    this.renderer2.appendChild(el, text);
    this.renderer2.appendChild(span, textSpan);
    this.renderer2.appendChild(el, span);
    this.renderer2.setAttribute(span,'style', 'cursor: pointer; float: right');
    this.renderer2.appendChild(div, el);
    this.renderer2.setAttribute(div,'style', 'display: flex');

    if(elemento === this.documentacion.nativeElement){
      this.renderer2.setAttribute(el,'style', 'width: 100%');
      this.renderer2.addClass(span,'ms-2');
    }
    else{
      this.renderer2.setAttribute(el,'style', 'width: 70px; margin-left: 10px;');
      this.renderer2.addClass(el,'tag');

    }

    this.renderer2.appendChild(elemento, div);

    return [div, span]
  }

  esconderModal(){
    this.documentacion.nativeElement.innerHTML = '';
    this.files = [];

    this.hideModal.emit(true);
  }

  crearIncidencia(){
    let documentos: { audios: string[], fotos: string[], videos: string[] } = {
      audios: [],
      fotos: [],
      videos: []
    };

    let localizacion = {
      audioLocalizacion: '',
      plano: ''
    }
    for(let file of this.files){
      if(file.name.includes('audio')){
        documentos.audios.push(file.name);
      }
      else if(file.name.includes('foto')){
        documentos.fotos.push(file.name);
      }
      else if(file.name.includes('video')){
        documentos.videos.push(file.name);
      }
      else if(file.name.includes('coordenadas')){
        localizacion.audioLocalizacion = file.name
      }
      else if(file.name.includes('plano')){
        localizacion.plano = file.name
      }
    }

    let nombre = '';
    this.authService.getName().subscribe(name => {
      nombre = name;
    });
    this.incidenciaForm.patchValue({
      autorNombre: nombre,
      autor: this.authService.getUid(),
      documentos: documentos,
      localizacion: localizacion,
      proyecto: this.proyecto
    });

    this.incidenciaService.crearIncidencia(this.incidenciaForm.value)
    .subscribe({
      next: res => {
        const formData = new FormData();
        for(let file of this.files){
          formData.append('files', file);
        }

        this.mediaService.uploadFile(formData)
        .subscribe({
          next: res => {
            this.incidenciaForm.markAsPristine();
            this.esconderModal();
            this.documentacion.nativeElement.innerHTML = '';
          },
          error: error => {
            Swal.fire({icon: 'error', title: 'Oops...', text: 'No se pudo subir el archivo'+error});
          }
        });
      },
      error: error => {
        Swal.fire({icon: 'error', title: 'Oops...', text: 'No se pudo crear la incidencia',});
      }
    });
  }
}
