import { AfterViewInit, Component, ElementRef, HostListener, OnInit, Renderer2, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { UsuarioService } from 'src/app/services/user.service';

@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.css']
})
export class SideBarComponent implements OnInit, AfterViewInit {
  ruta!: string;
  screenHeight!: number;
  screenWidth!: number;
  admin: boolean = false;

  @ViewChild('sideBar') sideBar !: ElementRef;


  ngOnInit(): void {
    this.checkPage();
    this.checkRol();
  }

  ngAfterViewInit(): void {
    this.getScreenSize();
  }

  constructor(
    private router: Router,
    private renderer2: Renderer2,
    private authService: AuthService
  ){}

  @HostListener('window:resize', ['$event'])
  getScreenSize() {
    this.renderer2.setStyle(this.sideBar.nativeElement, 'height', '92vh');
  }

  checkPage(){
    const ruta = this.router.url.split('/');
    this.ruta = ruta[1];
  }

  checkRol(){
    if(this.authService.getRol() === 'admin'){
      this.admin = true;
    }
  }


}
