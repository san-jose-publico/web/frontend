import { Component, ElementRef, EventEmitter, Input, Output, QueryList, Renderer2, SimpleChanges, ViewChild, ViewChildren } from '@angular/core';
import { Especialidad } from 'src/app/interfaces/Especialidad';
import { Incidencia } from 'src/app/interfaces/Incidencia';
import { Inspeccion } from 'src/app/interfaces/Inspeccion';
import { Usuario } from 'src/app/interfaces/Usuario';
import { EspecialidadService } from 'src/app/services/especialidad.service';
import { UsuarioService } from 'src/app/services/user.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-notify-form',
  templateUrl: './notify-form.component.html',
  styleUrls: ['./notify-form.component.css']
})
export class NotifyFormComponent {
  especialidades: Especialidad[] = [];
  emails: string[] = [];
  usuarios: Usuario[] = [];
  showWarning: boolean = false;

  @ViewChildren('op') op!: QueryList<any>;
  @ViewChild('email') email!: ElementRef;

  @Input() notificationData!: Incidencia | Inspeccion;
  @Input() notificationPdf!: FormData;
  @Input() pdf!: FormData;
  @Output() cancel = new EventEmitter<boolean>(false);

  constructor(
    private especialidadService: EspecialidadService,
    private usuarioService: UsuarioService,
    private renderer2: Renderer2,

  ) {
  }

  async ngOnInit(){
    await this.getEspecialidades();
    await this.getUsuarios();
  }

  ngOnChanges(changes: SimpleChanges) {
    if(changes['notificationPdf'] && changes['notificationPdf'].currentValue !== undefined && changes['notificationPdf'].currentValue !== null) {
      console.log(this.notificationPdf);
    }
  }

  async getEspecialidades() {
    return new Promise((resolve, reject) => {
      this.especialidadService.getEspecialidades()
        .subscribe({
          next: res => {
            this.especialidades = res.data;
            resolve(res);
          },
          error: error => {
            reject(error);
            Swal.fire({ icon: 'error', title: 'Oops...', text: 'No se pudo obtener las especialidades', });
          }
        });
    })
  }

  async getUsuarios() {
    return new Promise((resolve, reject) => {
      this.usuarioService.getUsuarios()
        .subscribe({
          next: res => {
            this.usuarios = res.data;
            resolve(res);
          },
          error: error => {
            reject(error);
            Swal.fire({ icon: 'error', title: 'Oops...', text: 'No se pudo obtener los usuarios', });
          }
        });
    })
  }

  async avisarIncidencia() {
    let groupCheked = false;

    for(let option of this.op){
      if(option.nativeElement.checked){
        groupCheked = true;
        break;
      }
    }

    if(!groupCheked && !this.email.nativeElement.value){
      this.showWarning = true;
      return;
    }

    let url = "https://mail.google.com/mail/?view=cm&fs=1&tf=1";
    let asunto = "Notificación de ";
    let tipo;

    if ("disciplinas" in this.notificationData) {
      asunto += `nueva incidencia - ${this.notificationData.codigo}`;
      tipo = 'Incidencia';
    }
    else {
      asunto += `nueva inspección - ${this.notificationData.codigo}`;
      tipo = 'Inspección';
    }

    let cuerpo = tipo + " creada por " + this.notificationData.autorNombre + ". \n\nDescripción \n" + this.notificationData.descripcion;
    url += "&su=" + encodeURIComponent(asunto) + "&body=" + encodeURIComponent(cuerpo);

    let destinatarios: string[] = [];

    for(let option of this.op){
      if(option.nativeElement.checked){
        let usuarios: Usuario[] = await this.getUsuariosEspecialidad(option.nativeElement.name);
        for(let user of usuarios){
          destinatarios.push(user.email);
        }
      }
    }

    if(this.emails.length > 0){
      destinatarios.push(...this.emails);
    }

    url += "&to=" + encodeURIComponent(destinatarios.join(','));
    // while(!this.notificationPdf){
    //   let xhr = new XMLHttpRequest();
    //   xhr.open('POST', url, true);
    //   xhr.send(this.notificationPdf);
    // }
    window.open(url);
    this.cancelNotification();
  }

  cancelNotification(){
    this.emails = [];
    this.email.nativeElement.value = '';
    for(let option of this.op){
      if(option.nativeElement.checked){
        option.nativeElement.checked = false;
      }
    }
    this.cancel.emit(true);
  }

  async getUsuariosEspecialidad(especialidad: string): Promise<Usuario[]> {
    return new Promise((resolve, reject) => {
      this.usuarioService.getUsuariosEspecialidad(especialidad)
        .subscribe({
          next: res => {
            resolve(res.data);
          },
          error: error => {
            reject(error);
            Swal.fire({ icon: 'error', title: 'Oops...', text: 'No se pudo mandar notificación de la incidencia', });
          }
        });
    })
  }

  addUserEmail(user: string){
    if(!user){
      return;
    }

    const emailRegex = /([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9_-]+)/;
    const matches = user.match(emailRegex);
    const email = matches![0];

    const emailFound = this.emails.find(correo => correo === email);

    if(!emailFound){
      this.emails.push(email);
    }

    this.email.nativeElement.value = '';
  }

  deleteEmailFromArray(email: string){
    this.emails.forEach((element,index)=>{
      if(element === email){
        this.emails.splice(index,1);
      }
    });
  }

  showElement(hide: boolean, elemento: HTMLDivElement, property?: string) {
    let hidenClass = 'visibility-hidden';
    let shownClass = 'visibility-visible';

    if (property) {
      hidenClass = 'display-none';
      shownClass = 'display-block';
    }

    if (hide) {
      this.renderer2.removeClass(elemento, hidenClass);
      this.renderer2.addClass(elemento, shownClass);
    }
    else {
      this.renderer2.removeClass(elemento, shownClass);
      this.renderer2.addClass(elemento, hidenClass);
    }
  }
}
