import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.css']
})
export class PaginationComponent {
  @Input() recordsTotal: number = 0;     // total de registros a paginar
  @Input() recordCurrent: number = 0;  // posicion actual dentro del total
  @Input() recordsPerPage: number = 15;   // número de registros a mostrar por página
  @Input() dataLoaded: boolean = false;

  @Output() changePage:EventEmitter<number> = new EventEmitter();

  public pageCurrent = 0;
  public pageLast = 0;
  public prepost = 2; // numero de páginas previas+posteriores+1
  public pages: number[] = [];
  public recordTo = 0;    // Indicará el hasta en el mensaje Mostrado de 1 a N del total

  constructor() { }

  ngOnInit(): void {
    this.calculatePages();
  }

  ngOnChanges(): void {
    this.calculatePages();
  }

  emitChangePage(nextPage: number) {
    if(nextPage < 1 || nextPage > this.pageLast || nextPage === this.pageCurrent){
      return;
    }
    else{
      this.changePage.emit(nextPage);
    }
  }

  calculatePages(){
    if(this.recordsTotal === 0){
      this.pageCurrent = 0;
      this.pageLast = 0;
      this.pages = [];
      return;
    }
    this.recordTo = (this.recordCurrent + this.recordsPerPage - 1 <= this.recordsTotal ? this.recordCurrent + this.recordsPerPage - 1 : this.recordsTotal);
    if(this.recordCurrent > this.recordsTotal){
      this.recordCurrent = this.recordsTotal;
    }
    this.pageCurrent = Math.trunc( this.recordCurrent / this.recordsPerPage) + 1;
    this.pageLast = Math.trunc((this.recordsTotal - 1) / this.recordsPerPage) + 1;

    const from = (this.pageCurrent - this.prepost > 0 ? this.pageCurrent - this.prepost : 1);
    const to = (this.pageCurrent + this.prepost < this.pageLast ? this.pageCurrent + this.prepost : this.pageLast);

    this.pages = [];
    for (let index = from; index <= to; index++){
      this.pages.push(index);
    }
  }


}
