import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { UsuarioService } from 'src/app/services/user.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit{
  rol!: string;
  isLoggedIn!: boolean;
  username: string = '';
  token: string  = '';
  uid: string  = '';


  constructor(
    private authService: AuthService,
    private router: Router,
    private usuarioService: UsuarioService
  ){

  }

  async ngOnInit(): Promise<void> {
    await this.checkLogin(() => {
      this.cargarUsuario();
    });
  }

  checkLogin(callback: () => void): void {
    this.authService.checkLogin().subscribe(isAuthenticated => {
      if (isAuthenticated) {
        callback();
      } else {
        this.isLoggedIn = false;
      }
    });
  }

  async cargarUsuario(): Promise<void> {
    while (this.authService.getToken() == null) {
      await new Promise(resolve => setTimeout(resolve, 1000));
      this.token = this.authService.getToken();
      this.uid = this.authService.getUid();
    }

    if(this.authService.getUid() && this.authService.getToken()){
      this.uid = this.authService.getUid();

      this.usuarioService.getUsuario(this.uid)
      .subscribe({
        next: res => {
          this.username = res.data.nombre;
          this.authService.setNombre(this.username + " " + res.data.apellidos);
          this.rol = res.data.rol;
          this.isLoggedIn = true;
        },
        error: error => {
          this.authService.logout();
        }
      });
    }
  }

  logout(): void {
    this.authService.logout();
    this.router.navigate(['/login']);
  }

  goHome(): void {
    this.router.navigate(['/home']);
  }
}
