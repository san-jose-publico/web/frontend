export interface Incidencia {
  uid: string,
  codigo: string,
  autor: string,
  fechaCreacion: Date,
  descripcion: string,
  estado: string,
  proyecto: string,
  disciplinas: string[],
  documentos: Documento,
  localizacion: Localizacion
  proyectoNombre?: string,
  proyectoCod?: string,
  complemento1?: string,
  complemento2?: string,
  autorNombre?: string,
  fechaEstadoCerrada?: Date,
  blockChainId?: string
}

interface Documento {
  audios: string[],
  fotos: string[],
  videos: string[]
}

interface Localizacion {
  audioLocalizacion: string,
  plano: string
}
