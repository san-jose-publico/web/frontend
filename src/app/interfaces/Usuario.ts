export interface Usuario {
  uid: string,
  email: string,
  username: string,
  password: string
  nombre: string,
  apellidos: string,
  rol: string,
  fechaAlta: Date,
  ultimoAcceso: Date,
  activo: boolean,
  especialidad: string,
}
