export interface Albaran {
  numero: string,
  foto: string,
  m3: string,
  proveedor: string,
  tipoHormigon: string,
  otro: string
}
