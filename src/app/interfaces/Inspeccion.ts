export interface Inspeccion {
  uid: string,
  codigo: string,
  autor: string,
  fechaCreacion: Date,
  descripcion: string,
  estado: string,
  proyecto: string,
  incidencia: string,
  documentos: Documento,
  complemento1?: string,
  complemento2?: string,
  incidenciaCod?: string,
  proyectoCod?: string,
  autorNombre?: string,
}


interface Documento {
  audios: string[],
  fotos: string[],
  videos: string[]
}
