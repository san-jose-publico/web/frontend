import { Incidencia } from "./Incidencia";
import { Inspeccion } from "./Inspeccion";

export interface Proyecto {
  uid: string,
  codigo: string,
  nombre: string,
  autor: string,
  fechaCreacion: Date,
  documentos: string[],
  numInc: string,
  numIns: string,
  incidencias?: Incidencia,
  inspecciones?: Inspeccion,
  autorNombre?: string,
}
