import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';
import { ProjectsComponent } from './pages/projects/projects.component';
import { IncidencesComponent } from './pages/incidences/incidences.component';
import { InspectionsComponent } from './pages/inspections/inspections.component';
import { ReportsComponent } from './pages/reports/reports.component';
import { UsersComponent } from './pages/users/users.component';
import { AuthGuard } from './services/auth.guard';
import { AdminGuard, rolGuard } from './services/admin.guard';
import { AlbaranesComponent } from './pages/albaranes/albaranes.component';

const routes: Routes = [
  { path: '', component: HomeComponent,  canActivate: [AuthGuard] },
  { path: 'login', component: LoginComponent },
  { path: 'home', component: HomeComponent, canActivate: [AuthGuard] },
  { path: 'proyectos', component: ProjectsComponent, canActivate: [AuthGuard] },
  { path: 'incidencias', component: IncidencesComponent, canActivate: [AuthGuard] },
  { path: 'inspecciones', component: InspectionsComponent, canActivate: [AuthGuard] },
  { path: 'informes', component: ReportsComponent, canActivate: [AuthGuard] },
  { path: 'usuarios', component: UsersComponent, canActivate: [AuthGuard, AdminGuard] },
  { path: 'albaranes', component: AlbaranesComponent, canActivate: [AuthGuard] },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
