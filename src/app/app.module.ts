import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { ProjectsComponent } from './pages/projects/projects.component';
import { IncidencesComponent } from './pages/incidences/incidences.component';
import { InspectionsComponent } from './pages/inspections/inspections.component';
import { LoginComponent } from './pages/login/login.component';
import { NavBarComponent } from './components/shared/nav-bar/nav-bar.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SideBarComponent } from './components/shared/side-bar/side-bar.component';
import { ReportsComponent } from './pages/reports/reports.component';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { HtppInterceptorService } from './services/htpp-interceptor.service';
import { AuthService } from './services/auth.service';
import { IncidenceFormComponent } from './components/shared/incidence-form/incidence-form.component';
import { UsersComponent } from './pages/users/users.component';
import { loginGuard } from './services/auth.guard';
import { rolGuard } from './services/admin.guard';
import { QRCodeModule } from 'angularx-qrcode';
import { AlbaranesComponent } from './pages/albaranes/albaranes.component';
import { PaginationComponent } from './components/shared/pagination/pagination.component';
import { NotifyFormComponent } from './components/shared/notify-form/notify-form.component';


// import { MatSlideToggleModule } from '@angular/material/slide-toggle';
// import {MatCardModule} from '@angular/material/card';
// import {MatInputModule} from '@angular/material/input';
// import {MatButtonModule} from '@angular/material/button';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ProjectsComponent,
    IncidencesComponent,
    InspectionsComponent,
    LoginComponent,
    NavBarComponent,
    SideBarComponent,
    ReportsComponent,
    IncidenceFormComponent,
    UsersComponent,
    AlbaranesComponent,
    PaginationComponent,
    NotifyFormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    QRCodeModule
    // MatSlideToggleModule,
    // MatCardModule,
    // MatInputModule,
    // MatButtonModule
  ],

  providers: [
    AuthService,
    loginGuard,
    rolGuard,
    {
    provide: HTTP_INTERCEPTORS,
    useClass: HtppInterceptorService,
    multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
