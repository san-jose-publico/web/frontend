import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { environment } from '../../environments/env';

@Injectable({
  providedIn: 'root'
})
export class InspeccionService {

  private URL = environment.apiURL + '/inspecciones';

  constructor(private http: HttpClient) { }

  getInspecciones(desde?: number, tipo?: string, orden?: string, fechaIni?: string, fechaFin?: string, filtro?: string, palabra?: string){
    if (!orden) {orden = 'desc'; }
    if (!fechaIni && !fechaFin) {
      fechaIni = '';
      fechaFin = '';
    }
    if(!tipo){
      tipo = 'fechaCreacion';
      filtro = '';
      palabra = '';
    }
    return this.http.get<any>(`${this.URL}?desde=${desde}&tipo=${tipo}&orden=${orden}&fechaIni=${fechaIni}&fechaFin=${fechaFin}&filtro=${filtro}&palabra=${palabra}`);
  }

  getInspeccionesProyecto(id: string, tipo?: string, orden?: string, fechaIni?: string, fechaFin?: string, filtro?: string, palabra?: string){
    // if (!desde) { desde = 0; }
    if (!orden) {orden = 'desc'; }
    if (!fechaIni && !fechaFin) {
      fechaIni = '';
      fechaFin = '';
    }
    if(!tipo){
      tipo = 'fechaCreacion';
      filtro = '';
      palabra = '';
    }
    return this.http.get<any>(`${this.URL}/proyecto/${id}?tipo=${tipo}&orden=${orden}&fechaIni=${fechaIni}&fechaFin=${fechaFin}&filtro=${filtro}&palabra=${palabra}`);
  }

  deleteInspeccion(id: string){
    return this.http.delete<any>(`${this.URL}/${id}`);
  }

}
