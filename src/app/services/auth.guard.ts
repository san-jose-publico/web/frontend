import { Injectable, inject } from '@angular/core';
import { CanActivateFn, Router } from '@angular/router';
import { AuthService } from './auth.service';


@Injectable()
 export class loginGuard {
  loggedIn: boolean = false;

  constructor(
    private authService: AuthService,
    public router: Router,
  ) { }

  canActivate(): boolean {
    this.authService.checkLogin().subscribe(isAuthenticated => {
      if (isAuthenticated) {
        this.loggedIn = true;
      } else {
        this.router.navigate(['/login']);
      }
    });

    return this.loggedIn
  }

 }

export const AuthGuard: CanActivateFn = (route, state) => {
  return inject(loginGuard).canActivate();
};
