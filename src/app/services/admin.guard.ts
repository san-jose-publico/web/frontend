import { Injectable, inject } from '@angular/core';
import { CanActivateFn, Router } from '@angular/router';
import { AuthService } from './auth.service';



@Injectable()
 export class rolGuard {
  loggedIn: boolean = false;

  constructor(
    private authService: AuthService,
    public router: Router,
  ) { }

  canActivate(): boolean {
    if(this.authService.getRol() === 'admin'){
      return true;
    }
    else{
      this.router.navigate(['/home']);
      return false;
    }
  }

 }

export const AdminGuard: CanActivateFn = (route, state) => {
  return inject(rolGuard).canActivate();
};
