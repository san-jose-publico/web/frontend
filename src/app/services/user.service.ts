import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { environment } from '../../environments/env';
import { tap, map, catchError } from 'rxjs/operators';
// import { User } from '../interfaces/user';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  private URL = environment.apiURL + '/usuarios';

  constructor(private http: HttpClient) { }

  getUsuario(uid: String){
    return this.http.get<any>(`${this.URL}/${uid}`);
  }

  getUsuarios(desde?: number, tipo?:string, orden?: string,  fechaIni?: string, fechaFin?: string, filtro?: string, palabra?: string){
    if (!orden) {orden = 'desc'; }
    if (!fechaIni && !fechaFin) {
      fechaIni = '';
      fechaFin = '';
    }
    if (!tipo){
      tipo = 'ultimoAcceso';
      filtro = '';
      palabra = '';
    }
    return this.http.get<any>(`${this.URL}?desde=${desde}&tipo=${tipo}&orden=${orden}&fechaIni=${fechaIni}&fechaFin=${fechaFin}&filtro=${filtro}&palabra=${palabra}`);
  }

  getUsuariosEspecialidad(especialidad: string){
    return this.http.get<any>(`${this.URL}/especialidad/${especialidad}`);
  }

  actualizarUsuario(uid: string, usuario: any){
    return this.http.put<any>(`${this.URL}/${uid}`, usuario);
  }

  statusUser(uid: string, data: any){
    return this.http.patch<any>(`${this.URL}/status/${uid}`, data);
  }

  actualizarUltimoAcceso(uid: string, data: any){
    return this.http.patch<any>(`${this.URL}/${uid}/ultimoAcceso`, data);
  }

  eliminarUsuario(uid: string) {
    return this.http.delete<any>(`${this.URL}/${uid}`);
  }

  createUser(usuario: any){
    return this.http.post<any>(this.URL, usuario);
  }

}
