import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { environment } from '../../environments/env';

@Injectable({
  providedIn: 'root'
})
export class MediaService {
  private URL = environment.apiURL;

  constructor(private http: HttpClient) {}


  getMediaFiles(){
    return this.http.get<any>(`${this.URL}/media/list`);
  }

  getFile(name: string){
    return this.http.get(`${this.URL}/media/download/${name}`, {responseType: 'text'} );
  }

  deleteFile(name: string){
    return this.http.delete(`${this.URL}/media/${name}`, {responseType: 'text'});
  }

  uploadFile(file: FormData){
    return this.http.post(`${this.URL}/media`, file, { responseType: 'text' });
  }
}
