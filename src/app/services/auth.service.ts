import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { environment } from 'src/environments/env';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private URL = environment.apiURL;
  private isAuthenticated = new BehaviorSubject<boolean>(false);
  private name = new BehaviorSubject<string>('');

  constructor(private http: HttpClient){
    const isAuthenticated = localStorage.getItem('token');
    if(isAuthenticated) {
      this.isAuthenticated.next(true);
    }
  }

  login(user: any) {
    this.isAuthenticated.next(true);
    return this.http.post<any>(this.URL + '/usuarios/login', user);
  }

  logout() {
    this.isAuthenticated.next(false);
    localStorage.removeItem('token');
    localStorage.removeItem('uid');
    localStorage.removeItem('rol');

  }

  checkLogin(): Observable<boolean> {
    return this.isAuthenticated.asObservable();
  }

  getToken() {
    return localStorage.getItem('token') as any;
  }

  getRol() {
    return localStorage.getItem('rol') as any;
  }

  getUid() {
    return localStorage.getItem('uid') as any;
  }

  setNombre(name: string){
    this.name.next(name);
  }

  getName(){
    return this.name;
  }


}
