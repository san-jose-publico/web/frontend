import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { environment } from '../../environments/env';

@Injectable({
  providedIn: 'root'
})
export class IncidenciaService {
  private URL = environment.apiURL + '/incidencias';

  constructor(private http: HttpClient) {}

  getIncidencias(desde?: number, tipo?: string, orden?: string, fechaIni?: string, fechaFin?: string, filtro?: string, palabra?: string){
    if (!orden) {orden = 'desc'; }
    if (!fechaIni && !fechaFin) {
      fechaIni = '';
      fechaFin = '';
    }
    if(!tipo){
      tipo = 'fechaCreacion';
      filtro = '';
      palabra = '';
    }
    return this.http.get<any>(`${this.URL}?desde=${desde}&tipo=${tipo}&orden=${orden}&fechaIni=${fechaIni}&fechaFin=${fechaFin}&filtro=${filtro}&palabra=${palabra}`);
  }

  getIncidenciasAll(){
    return this.http.get<any>(`${this.URL}/all`);
  }

  getIncidencia(id: string) {
    return this.http.get<any>(`${this.URL}/${id}`);
  }

  getIncidenciasProyecto(id: string, tipo?: string, orden?: string, fechaIni?: string, fechaFin?: string, filtro?: string, palabra?: string){
    // if (!desde) { desde = 0; }
    if (!orden) {orden = 'desc'; }
    if (!fechaIni && !fechaFin) {
      fechaIni = '';
      fechaFin = '';
    }
    if(!tipo){
      tipo = 'fechaCreacion';
      filtro = '';
      palabra = '';
    }
    return this.http.get<any>(`${this.URL}/proyecto/${id}?tipo=${tipo}&orden=${orden}&fechaIni=${fechaIni}&fechaFin=${fechaFin}&filtro=${filtro}&palabra=${palabra}`);
  }

  getInspeccionesFromIncidencia(id: string, tipo?: string, orden?: string, fechaIni?: string, fechaFin?: string, filtro?: string, palabra?: string){
    // if (!desde) { desde = 0; }
    if (!orden) {orden = 'desc'; }
    if (!fechaIni && !fechaFin) {
      fechaIni = '';
      fechaFin = '';
    }
    if(!tipo){
      tipo = 'fechaCreacion';
      filtro = '';
      palabra = '';
    }
    return this.http.get<any>(`${this.URL}/inspecciones/${id}?tipo=${tipo}&orden=${orden}&fechaIni=${fechaIni}&fechaFin=${fechaFin}&filtro=${filtro}&palabra=${palabra}`);
  }

  crearIncidencia<T>(data: T){
    return this.http.post<any>(`${this.URL}`, data);
  }

  updateStatus<T>(id: string, data: T){
    return this.http.patch<any>(`${this.URL}/${id}/status`, data);
  }

  setBlockChainId<T>(id: string, blockChainId: any, data: T){
    return this.http.patch<any>(`${this.URL}/${id}/blockChainId/${blockChainId}`, data);
  }

  deleteIncidencia(id: string){
    return this.http.delete<any>(`${this.URL}/${id}`);
  }
}
