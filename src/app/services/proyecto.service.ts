import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { environment } from '../../environments/env';

@Injectable({
  providedIn: 'root'
})
export class ProyectoService {
 private URL = environment.apiURL + '/proyectos';

  constructor(private http: HttpClient) { }

  getProyectos(desde?: number, tipo?: string, orden?: string, fechaIni?: string, fechaFin?: string, filtro?: string, palabra?: string){
    if (!orden) {orden = 'desc'; }
    if (!fechaIni && !fechaFin) {
      fechaIni = '';
      fechaFin = '';
    }
    if(!tipo){
      tipo = 'fechaCreacion';
      filtro = '';
      palabra = '';
    }
    return this.http.get<any>(`${this.URL}?desde=${desde}&tipo=${tipo}&orden=${orden}&fechaIni=${fechaIni}&fechaFin=${fechaFin}&filtro=${filtro}&palabra=${palabra}`);
  }

  getProyectosAll(){
    return this.http.get<any>(`${this.URL}/all`);
  }

  crearProyecto<T>(data: T){
    return this.http.post<any>(`${this.URL}`, data);
  }

  getProyecto(uid: string){
    return this.http.get<any>(`${this.URL}/${uid}`);
  }

  editarDocumentacionProyecto<T>(uid: string, data: T){
    return this.http.patch<any>(`${this.URL}/${uid}`, data);
  }

  actualizarProyecto<T>(uid: string, data: T){
    return this.http.put<any>(`${this.URL}/${uid}`, data);
  }



}
