import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/env';
import { Especialidad } from '../interfaces/Especialidad';

@Injectable({
  providedIn: 'root',
})
export class EspecialidadService {
  private URL = environment.apiURL + '/especialidades';

  constructor(private http: HttpClient) {}

  getEspecialidades() {
    return this.http.get<any>(`${this.URL}`);
  }

  createEspecialidad(data: Especialidad) {
    return this.http.post<any>(`${this.URL}`, data);
  }

  deleteEspecialidad(uid: string) {
    return this.http.delete<any>(`${this.URL}/${uid}`);
  }
}
