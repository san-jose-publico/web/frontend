import { Injectable } from '@angular/core';
import { HttpHandler, HttpErrorResponse, HttpEvent, HttpRequest } from '@angular/common/http';
import { Router } from '@angular/router';
import { catchError, Observable, of } from 'rxjs';
import { AuthService } from './auth.service';
// import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class HtppInterceptorService {
  constructor(
    private router: Router,
    private authService: AuthService,
    // private _snackBar: MatSnackBar
    ) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    request = request.clone({
      setHeaders: {
        token: `${this.authService.getToken()}`,
        rol: `${this.authService.getRol()}`,
        uid: `${this.authService.getUid()}`,
      }
    });

    //Si la solicitud es la subida de modelos a sketchfab, no se puede enviar las cabeceras extra
    //porque si no da un error de cors
    const isCodeContractRequest = request.url.includes('codecontractplattform');
    if (isCodeContractRequest) {
      request = request.clone({
        headers: request.headers.delete('token').delete('rol').delete('uid')
      });
    }

    return next.handle(request).pipe(
      catchError(
        (err) => {
          console.log(err);

          if (err.status === 401) {
            this.authService.logout();
            this.router.navigate(['/login']);
            return of(err);
          }
          throw err;
        }
      )
    );
  };
}
