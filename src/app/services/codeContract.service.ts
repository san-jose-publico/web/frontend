import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, map } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class codeContractService {
  private apiURL = 'https://api.codecontractplattform.com/api';
  private email = "xabier.angulo@bexreal.com";
  private password = "Be20xR3A124";

  constructor(private http: HttpClient) {}

  login() {
    const body = {
      email: this.email,
      password: this.password
    }
    return this.http.post<any>(`${this.apiURL}/login`, body);
  }

  registerObject(token: string, body: any){
    const headers = new HttpHeaders().set('Authorization', `Bearer ${token}`);

    return this.http.post<any>(`${this.apiURL}/smart-check/create-tree-and-register-merkle-root`, body, { headers });
  }

  downloadObject(token: string, body: any, name: string) {
    const headers = new HttpHeaders().set('Authorization', `Bearer ${token}`);

    return this.http.post(`${this.apiURL}/smart-check/print-reports`, body, {
      headers,
      responseType: 'blob'
    }).pipe(
      map((data: Blob) => {
        const blob = new Blob([data], { type: 'application/zip' });

        // Crea un objeto URL para el blob descargado
        const url = window.URL.createObjectURL(blob);

        // Crea un enlace temporal y simula un clic para descargar el archivo
        const a = document.createElement('a');
        a.href = url;
        a.download = name;
        document.body.appendChild(a);
        a.click();
        document.body.removeChild(a);

        // Limpia el objeto URL
        window.URL.revokeObjectURL(url);
      }),
      catchError((error) => {
        console.error('Error al descargar el archivo', error);
        throw error;
      })
    );
  }
}
