import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/env';
import { Albaran } from '../interfaces/Albaran';

@Injectable({
  providedIn: 'root'
})
export class AlbaranService {
  private URL = environment.apiURL + '/albaranes';

  constructor(private http: HttpClient) {}

  getAlbaranes(desde?: number, tipo?: string, orden?: string, fechaIni?: string, fechaFin?: string, filtro?: string, palabra?: string){
    if (!orden) {orden = 'desc'; }
    if (!fechaIni && !fechaFin) {
      fechaIni = '';
      fechaFin = '';
    }
    if(!tipo){
      tipo = 'fechaCreacion';
      filtro = '';
      palabra = '';
    }
    return this.http.get<any>(`${this.URL}?desde=${desde}&tipo=${tipo}&orden=${orden}&fechaIni=${fechaIni}&fechaFin=${fechaFin}&filtro=${filtro}&palabra=${palabra}`);
  }

  createAlbaran(data: Albaran) {
    return this.http.post<any>(`${this.URL}`, data);
  }

  deleteAlbaran(uid: string) {
    return this.http.delete<any>(`${this.URL}/${uid}`);
  }
}
